﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IndividualWorkKrylovP_81
{
    class Student
    {
        private string fullName;
        private uint yearOfBirth;
        private uint groupNum;
        private uint[] marks;
        private double averageMark;

        public Student()
        {
            fullName = "";
            yearOfBirth = 0;
            groupNum = 0;
            marks = null;
            averageMark = 0;
        }

        public Student(string fullName, uint yearOfBirth, uint groupNum, uint[] marks)
        {
            this.fullName = fullName;
            this.yearOfBirth = yearOfBirth;
            this.groupNum = groupNum;
            this.marks = marks;
                    
            foreach(var mark in marks)
            {
                averageMark += mark;
            }

            averageMark /= marks.Length;
        }

        public string getFullName()
        {
            return fullName;
        }

        public uint getYearOfBith()
        {
            return yearOfBirth;
        }

        public uint getGroupNum()
        {
            return groupNum;
        }

        public uint[] getMarks()
        {
            return marks;
        }

        public double getAverageMark()
        {
            return averageMark;
        }

        public string setFullName()
        {
            return fullName;
        }

        public void setYearOfBith(uint yearOfBirth)
        {
            this.yearOfBirth = yearOfBirth;
        }

        public void setGroupNum(uint groupNum)
        {
            this.groupNum = groupNum;
        }

        public void setMarks(uint[] marks)
        {
            this.marks = marks;
        }

        public void setAverageMark(double averageMark)
        {
            this.averageMark = averageMark;
        }

    }
}
