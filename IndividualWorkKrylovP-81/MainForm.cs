﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IndividualWorkKrylovP_81
{
    public partial class MainForm : Form
    {
        List<Student> listOfStudents = new List<Student>();

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Student student = new Student(
                tbFullName.Text,
                UInt16.Parse(tbYearOfBirth.Text),
                UInt32.Parse(tbGroupNum.Text),
                Array.ConvertAll(tbMarks.Text.Split(','), uint.Parse)
                );

            listOfStudents.Add(student);

            MessageBox.Show("Запис успішно додано!");

            dgv.Rows.Add(tbFullName.Text, tbYearOfBirth.Text, tbGroupNum.Text, tbMarks.Text, student.getAverageMark().ToString());

            tbFullName.Text = "";
            tbYearOfBirth.Text = "";
            tbGroupNum.Text = "";
            tbMarks.Text = "";
        }

        private void btnSort_Click(object sender, EventArgs e)
        {
            double totalAverageMark = 0;

            foreach(var student in listOfStudents)
            {
                totalAverageMark += student.getAverageMark();
            }

            totalAverageMark /= listOfStudents.Count;

            dgv.Rows.Clear();

            foreach(var student in listOfStudents)
            {
                if(student.getAverageMark() > totalAverageMark)
                {
                    dgv.Rows.Add(
                        student.getFullName(),
                        student.getYearOfBith(),
                        student.getGroupNum(),
                        string.Join(",", student.getMarks()),
                        student.getAverageMark()
                        );
                }
            }
        }

        private void btnSaveChanges_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dgv.Rows.Count; i++)
            {
                for(int j = 0; j < listOfStudents.Count; j++)
                {
                    if (dgv.Rows[i].Cells[1].Value.ToString().Equals(listOfStudents[j].getFullName()))
                    {
                        listOfStudents[j].setYearOfBith(UInt16.Parse(dgv.Rows[i].Cells[2].Value.ToString()));
                        listOfStudents[j].setGroupNum(UInt16.Parse(dgv.Rows[i].Cells[3].Value.ToString()));
                        listOfStudents[j].setMarks(Array.ConvertAll(dgv.Rows[i].Cells[4].Value.ToString().Split(','), uint.Parse));
                        listOfStudents[j].setAverageMark(Double.Parse(dgv.Rows[i].Cells[5].Value.ToString()));
                    }
                }
            }
        }
    }
}
