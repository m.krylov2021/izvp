﻿
namespace IZVPProject1
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.autoInsert = new System.Windows.Forms.Button();
            this.sizeTextBox = new System.Windows.Forms.TextBox();
            this.createButton = new System.Windows.Forms.Button();
            this.saveBtn = new System.Windows.Forms.Button();
            this.calculateSumRowsBtn = new System.Windows.Forms.Button();
            this.labelMaxDiag = new System.Windows.Forms.Label();
            this.findMaxDiagBtn = new Guna.UI2.WinForms.Guna2Button();
            this.dgv = new Guna.UI2.WinForms.Guna2DataGridView();
            this.calculateMultColumnsBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.exitBtn = new Guna.UI2.WinForms.Guna2Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // autoInsert
            // 
            this.autoInsert.BackColor = System.Drawing.Color.DodgerBlue;
            this.autoInsert.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.autoInsert.Location = new System.Drawing.Point(475, 181);
            this.autoInsert.Name = "autoInsert";
            this.autoInsert.Size = new System.Drawing.Size(173, 53);
            this.autoInsert.TabIndex = 1;
            this.autoInsert.Text = "Автоматичне заповнення";
            this.autoInsert.UseVisualStyleBackColor = false;
            this.autoInsert.Click += new System.EventHandler(this.autoInsert_Click);
            // 
            // sizeTextBox
            // 
            this.sizeTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sizeTextBox.Location = new System.Drawing.Point(502, 92);
            this.sizeTextBox.Name = "sizeTextBox";
            this.sizeTextBox.Size = new System.Drawing.Size(117, 26);
            this.sizeTextBox.TabIndex = 2;
            // 
            // createButton
            // 
            this.createButton.BackColor = System.Drawing.Color.DarkOrange;
            this.createButton.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.createButton.Location = new System.Drawing.Point(475, 134);
            this.createButton.Name = "createButton";
            this.createButton.Size = new System.Drawing.Size(173, 39);
            this.createButton.TabIndex = 4;
            this.createButton.Text = "Створити масив";
            this.createButton.UseVisualStyleBackColor = false;
            this.createButton.Click += new System.EventHandler(this.createButton_Click);
            // 
            // saveBtn
            // 
            this.saveBtn.BackColor = System.Drawing.Color.ForestGreen;
            this.saveBtn.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.saveBtn.Location = new System.Drawing.Point(126, 456);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(151, 41);
            this.saveBtn.TabIndex = 5;
            this.saveBtn.Text = "Зберегти дані";
            this.saveBtn.UseVisualStyleBackColor = false;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // calculateSumRowsBtn
            // 
            this.calculateSumRowsBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.calculateSumRowsBtn.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.calculateSumRowsBtn.Location = new System.Drawing.Point(475, 243);
            this.calculateSumRowsBtn.Name = "calculateSumRowsBtn";
            this.calculateSumRowsBtn.Size = new System.Drawing.Size(173, 55);
            this.calculateSumRowsBtn.TabIndex = 6;
            this.calculateSumRowsBtn.Text = "Розрахувати суму кожного рядка";
            this.calculateSumRowsBtn.UseVisualStyleBackColor = false;
            this.calculateSumRowsBtn.Click += new System.EventHandler(this.calculateSumRowsBtn_Click);
            // 
            // labelMaxDiag
            // 
            this.labelMaxDiag.AutoSize = true;
            this.labelMaxDiag.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelMaxDiag.Location = new System.Drawing.Point(443, 408);
            this.labelMaxDiag.Name = "labelMaxDiag";
            this.labelMaxDiag.Size = new System.Drawing.Size(205, 17);
            this.labelMaxDiag.TabIndex = 7;
            this.labelMaxDiag.Text = "max елемент гол. діагоналі = ";
            // 
            // findMaxDiagBtn
            // 
            this.findMaxDiagBtn.Animated = true;
            this.findMaxDiagBtn.BorderRadius = 8;
            this.findMaxDiagBtn.CheckedState.Parent = this.findMaxDiagBtn;
            this.findMaxDiagBtn.CustomImages.Parent = this.findMaxDiagBtn;
            this.findMaxDiagBtn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.findMaxDiagBtn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.findMaxDiagBtn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.findMaxDiagBtn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.findMaxDiagBtn.DisabledState.Parent = this.findMaxDiagBtn;
            this.findMaxDiagBtn.FillColor = System.Drawing.Color.IndianRed;
            this.findMaxDiagBtn.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.findMaxDiagBtn.ForeColor = System.Drawing.Color.White;
            this.findMaxDiagBtn.HoverState.Parent = this.findMaxDiagBtn;
            this.findMaxDiagBtn.Location = new System.Drawing.Point(475, 447);
            this.findMaxDiagBtn.Name = "findMaxDiagBtn";
            this.findMaxDiagBtn.ShadowDecoration.Parent = this.findMaxDiagBtn;
            this.findMaxDiagBtn.Size = new System.Drawing.Size(173, 50);
            this.findMaxDiagBtn.TabIndex = 8;
            this.findMaxDiagBtn.Text = "Знайти max головної діагоналі";
            this.findMaxDiagBtn.Click += new System.EventHandler(this.findMaxDiagBtn_Click);
            // 
            // dgv
            // 
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle10;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgv.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Segoe UI", 10.5F);
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dgv.ColumnHeadersHeight = 45;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Segoe UI", 10.5F);
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv.DefaultCellStyle = dataGridViewCellStyle12;
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv.Location = new System.Drawing.Point(12, 12);
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(402, 438);
            this.dgv.TabIndex = 9;
            this.dgv.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.dgv.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgv.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.dgv.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.dgv.ThemeStyle.BackColor = System.Drawing.Color.White;
            this.dgv.ThemeStyle.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            this.dgv.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Segoe UI", 10.5F);
            this.dgv.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dgv.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv.ThemeStyle.HeaderStyle.Height = 45;
            this.dgv.ThemeStyle.ReadOnly = false;
            this.dgv.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgv.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Segoe UI", 10.5F);
            this.dgv.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgv.ThemeStyle.RowsStyle.Height = 22;
            this.dgv.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            // 
            // calculateMultColumnsBtn
            // 
            this.calculateMultColumnsBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.calculateMultColumnsBtn.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.calculateMultColumnsBtn.Location = new System.Drawing.Point(475, 307);
            this.calculateMultColumnsBtn.Name = "calculateMultColumnsBtn";
            this.calculateMultColumnsBtn.Size = new System.Drawing.Size(173, 55);
            this.calculateMultColumnsBtn.TabIndex = 10;
            this.calculateMultColumnsBtn.Text = "Розрахувати добуток кожного стовпця";
            this.calculateMultColumnsBtn.UseVisualStyleBackColor = false;
            this.calculateMultColumnsBtn.Click += new System.EventHandler(this.calculateMultColumnsBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(444, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(232, 21);
            this.label1.TabIndex = 11;
            this.label1.Text = "Введіть розмірність матриці:";
            // 
            // exitBtn
            // 
            this.exitBtn.CheckedState.Parent = this.exitBtn;
            this.exitBtn.CustomImages.Parent = this.exitBtn;
            this.exitBtn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.exitBtn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.exitBtn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.exitBtn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.exitBtn.DisabledState.Parent = this.exitBtn;
            this.exitBtn.FillColor = System.Drawing.Color.White;
            this.exitBtn.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.exitBtn.ForeColor = System.Drawing.Color.White;
            this.exitBtn.HoverState.Parent = this.exitBtn;
            this.exitBtn.Image = ((System.Drawing.Image)(resources.GetObject("exitBtn.Image")));
            this.exitBtn.ImageSize = new System.Drawing.Size(30, 30);
            this.exitBtn.Location = new System.Drawing.Point(668, 7);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.ShadowDecoration.Parent = this.exitBtn;
            this.exitBtn.Size = new System.Drawing.Size(41, 38);
            this.exitBtn.TabIndex = 12;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(718, 515);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.calculateMultColumnsBtn);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.findMaxDiagBtn);
            this.Controls.Add(this.labelMaxDiag);
            this.Controls.Add(this.calculateSumRowsBtn);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.createButton);
            this.Controls.Add(this.sizeTextBox);
            this.Controls.Add(this.autoInsert);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button autoInsert;
        private System.Windows.Forms.TextBox sizeTextBox;
        private System.Windows.Forms.Button createButton;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Button calculateSumRowsBtn;
        private System.Windows.Forms.Label labelMaxDiag;
        private Guna.UI2.WinForms.Guna2Button findMaxDiagBtn;
        private Guna.UI2.WinForms.Guna2DataGridView dgv;
        private System.Windows.Forms.Button calculateMultColumnsBtn;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2Button exitBtn;
    }
}

