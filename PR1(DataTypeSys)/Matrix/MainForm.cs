﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IZVPProject1
{
    public partial class MainForm : Form
    {
        int[,] myArray = new int[1, 1];
        int size;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
           
        }

        private void autoInsert_Click(object sender, EventArgs e)
        {
            Random rand = new Random();

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    dgv.Rows[i].Cells[j].Value = rand.Next(50);
                    myArray[i, j] = Convert.ToInt32(dgv.Rows[i].Cells[j].Value);
                }
            }
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < size; i++)
                {
                    for (int j = 0; j < size; j++)
                    {
                        myArray[i, j] = Convert.ToInt32(dgv.Rows[i].Cells[j].Value);
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Виникла наступна помилка: " + ex);
            }
        }

        private void calculateSumRowsBtn_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < size; i++)
            {
                int sum = 0;

                for (int j = 0; j < size; j++)
                {
                    sum += Convert.ToInt32(dgv.Rows[i].Cells[j].Value);
                }

                dgv.Rows[i].Cells[size].Value = sum;
            }
        }

        private void findMaxDiagBtn_Click(object sender, EventArgs e)
        {
            int max = myArray[0, 0];
            for (int i = 1; i < size; i++)
            {
                if(max < myArray[i, i])
                {
                    max = myArray[i, i];
                }
            }

            labelMaxDiag.Text = "max елемент гол. діагоналі = " + max;
        }

        private void createButton_Click(object sender, EventArgs e)
        {

            size = Convert.ToInt32(sizeTextBox.Text.ToString());

            int[,] mass = new int[size, size];

            dgv.RowCount = size + 1;
            dgv.ColumnCount = size + 1;

            dgv.Columns[size].HeaderText = "Сума кожного рядка";

            myArray = mass;


            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    myArray[i, j] = 1;
                    dgv.Rows[i].Cells[j].Value = myArray[i, j];
                }
            }
                    
        }

        private void calculateMultColumnsBtn_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < size; i++)
            {
                int mult = 1;

                for (int j = 0; j < size; j++)
                {
                    mult *= Convert.ToInt32(dgv.Rows[j].Cells[i].Value);
                }

                dgv.Rows[size].Cells[i].Value = mult;
            }
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
