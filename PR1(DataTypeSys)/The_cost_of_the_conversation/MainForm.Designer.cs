﻿
namespace IZVP2_Krylov
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.exitBtn = new Guna.UI2.WinForms.Guna2Button();
            this.label1 = new System.Windows.Forms.Label();
            this.guna2Panel1 = new Guna.UI2.WinForms.Guna2Panel();
            this.calculateBtn = new Guna.UI2.WinForms.Guna2Button();
            this.tariffTextBox = new Guna.UI2.WinForms.Guna2TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.preferentialTariffTextBox = new Guna.UI2.WinForms.Guna2TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.numberOfGraceMinutesTextBox = new Guna.UI2.WinForms.Guna2TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.durationTextBox = new Guna.UI2.WinForms.Guna2TextBox();
            this.labelTotalPrice = new System.Windows.Forms.Label();
            this.guna2Panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // exitBtn
            // 
            this.exitBtn.CheckedState.Parent = this.exitBtn;
            this.exitBtn.CustomImages.Parent = this.exitBtn;
            this.exitBtn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.exitBtn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.exitBtn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.exitBtn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.exitBtn.DisabledState.Parent = this.exitBtn;
            this.exitBtn.FillColor = System.Drawing.Color.White;
            this.exitBtn.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.exitBtn.ForeColor = System.Drawing.Color.White;
            this.exitBtn.HoverState.Parent = this.exitBtn;
            this.exitBtn.Image = ((System.Drawing.Image)(resources.GetObject("exitBtn.Image")));
            this.exitBtn.Location = new System.Drawing.Point(291, 5);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.ShadowDecoration.Parent = this.exitBtn;
            this.exitBtn.Size = new System.Drawing.Size(36, 24);
            this.exitBtn.TabIndex = 0;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(46, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(209, 40);
            this.label1.TabIndex = 1;
            this.label1.Text = "Введіть тривалість розмови\r\n(в хвилинах):";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // guna2Panel1
            // 
            this.guna2Panel1.BackColor = System.Drawing.Color.LightGray;
            this.guna2Panel1.Controls.Add(this.calculateBtn);
            this.guna2Panel1.Controls.Add(this.tariffTextBox);
            this.guna2Panel1.Controls.Add(this.label4);
            this.guna2Panel1.Controls.Add(this.preferentialTariffTextBox);
            this.guna2Panel1.Controls.Add(this.label3);
            this.guna2Panel1.Controls.Add(this.numberOfGraceMinutesTextBox);
            this.guna2Panel1.Controls.Add(this.label2);
            this.guna2Panel1.Controls.Add(this.durationTextBox);
            this.guna2Panel1.Controls.Add(this.label1);
            this.guna2Panel1.Location = new System.Drawing.Point(12, 34);
            this.guna2Panel1.Name = "guna2Panel1";
            this.guna2Panel1.ShadowDecoration.Parent = this.guna2Panel1;
            this.guna2Panel1.Size = new System.Drawing.Size(308, 425);
            this.guna2Panel1.TabIndex = 2;
            // 
            // calculateBtn
            // 
            this.calculateBtn.Animated = true;
            this.calculateBtn.BorderRadius = 8;
            this.calculateBtn.CheckedState.Parent = this.calculateBtn;
            this.calculateBtn.CustomImages.Parent = this.calculateBtn;
            this.calculateBtn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.calculateBtn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.calculateBtn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.calculateBtn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.calculateBtn.DisabledState.Parent = this.calculateBtn;
            this.calculateBtn.FillColor = System.Drawing.Color.YellowGreen;
            this.calculateBtn.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.calculateBtn.ForeColor = System.Drawing.Color.White;
            this.calculateBtn.HoverState.Parent = this.calculateBtn;
            this.calculateBtn.Location = new System.Drawing.Point(66, 371);
            this.calculateBtn.Name = "calculateBtn";
            this.calculateBtn.ShadowDecoration.Parent = this.calculateBtn;
            this.calculateBtn.Size = new System.Drawing.Size(170, 45);
            this.calculateBtn.TabIndex = 4;
            this.calculateBtn.Text = "Розрахувати";
            this.calculateBtn.Click += new System.EventHandler(this.calculateBtn_Click);
            // 
            // tariffTextBox
            // 
            this.tariffTextBox.BackColor = System.Drawing.Color.LightSkyBlue;
            this.tariffTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tariffTextBox.DefaultText = "";
            this.tariffTextBox.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.tariffTextBox.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.tariffTextBox.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.tariffTextBox.DisabledState.Parent = this.tariffTextBox;
            this.tariffTextBox.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.tariffTextBox.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.tariffTextBox.FocusedState.Parent = this.tariffTextBox;
            this.tariffTextBox.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tariffTextBox.ForeColor = System.Drawing.Color.Black;
            this.tariffTextBox.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.tariffTextBox.HoverState.Parent = this.tariffTextBox;
            this.tariffTextBox.Location = new System.Drawing.Point(48, 320);
            this.tariffTextBox.Name = "tariffTextBox";
            this.tariffTextBox.PasswordChar = '\0';
            this.tariffTextBox.PlaceholderText = "";
            this.tariffTextBox.SelectedText = "";
            this.tariffTextBox.ShadowDecoration.Parent = this.tariffTextBox;
            this.tariffTextBox.Size = new System.Drawing.Size(205, 36);
            this.tariffTextBox.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(87, 295);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 20);
            this.label4.TabIndex = 7;
            this.label4.Text = "Введіть тариф:";
            // 
            // preferentialTariffTextBox
            // 
            this.preferentialTariffTextBox.BackColor = System.Drawing.Color.LightSkyBlue;
            this.preferentialTariffTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.preferentialTariffTextBox.DefaultText = "";
            this.preferentialTariffTextBox.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.preferentialTariffTextBox.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.preferentialTariffTextBox.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.preferentialTariffTextBox.DisabledState.Parent = this.preferentialTariffTextBox;
            this.preferentialTariffTextBox.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.preferentialTariffTextBox.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.preferentialTariffTextBox.FocusedState.Parent = this.preferentialTariffTextBox;
            this.preferentialTariffTextBox.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.preferentialTariffTextBox.ForeColor = System.Drawing.Color.Black;
            this.preferentialTariffTextBox.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.preferentialTariffTextBox.HoverState.Parent = this.preferentialTariffTextBox;
            this.preferentialTariffTextBox.Location = new System.Drawing.Point(50, 248);
            this.preferentialTariffTextBox.Name = "preferentialTariffTextBox";
            this.preferentialTariffTextBox.PasswordChar = '\0';
            this.preferentialTariffTextBox.PlaceholderText = "";
            this.preferentialTariffTextBox.SelectedText = "";
            this.preferentialTariffTextBox.ShadowDecoration.Parent = this.preferentialTariffTextBox;
            this.preferentialTariffTextBox.Size = new System.Drawing.Size(205, 36);
            this.preferentialTariffTextBox.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(87, 202);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(135, 40);
            this.label3.TabIndex = 5;
            this.label3.Text = "Введіть тариф за\r\nпільгові хвилини:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numberOfGraceMinutesTextBox
            // 
            this.numberOfGraceMinutesTextBox.BackColor = System.Drawing.Color.LightSkyBlue;
            this.numberOfGraceMinutesTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.numberOfGraceMinutesTextBox.DefaultText = "";
            this.numberOfGraceMinutesTextBox.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.numberOfGraceMinutesTextBox.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.numberOfGraceMinutesTextBox.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.numberOfGraceMinutesTextBox.DisabledState.Parent = this.numberOfGraceMinutesTextBox;
            this.numberOfGraceMinutesTextBox.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.numberOfGraceMinutesTextBox.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.numberOfGraceMinutesTextBox.FocusedState.Parent = this.numberOfGraceMinutesTextBox;
            this.numberOfGraceMinutesTextBox.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numberOfGraceMinutesTextBox.ForeColor = System.Drawing.Color.Black;
            this.numberOfGraceMinutesTextBox.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.numberOfGraceMinutesTextBox.HoverState.Parent = this.numberOfGraceMinutesTextBox;
            this.numberOfGraceMinutesTextBox.Location = new System.Drawing.Point(50, 153);
            this.numberOfGraceMinutesTextBox.Name = "numberOfGraceMinutesTextBox";
            this.numberOfGraceMinutesTextBox.PasswordChar = '\0';
            this.numberOfGraceMinutesTextBox.PlaceholderText = "";
            this.numberOfGraceMinutesTextBox.SelectedText = "";
            this.numberOfGraceMinutesTextBox.ShadowDecoration.Parent = this.numberOfGraceMinutesTextBox;
            this.numberOfGraceMinutesTextBox.Size = new System.Drawing.Size(205, 36);
            this.numberOfGraceMinutesTextBox.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(87, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 40);
            this.label2.TabIndex = 3;
            this.label2.Text = "Введіть кіл-ть\r\nпільгових хвилин:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // durationTextBox
            // 
            this.durationTextBox.BackColor = System.Drawing.Color.LightSkyBlue;
            this.durationTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.durationTextBox.DefaultText = "";
            this.durationTextBox.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.durationTextBox.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.durationTextBox.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.durationTextBox.DisabledState.Parent = this.durationTextBox;
            this.durationTextBox.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.durationTextBox.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.durationTextBox.FocusedState.Parent = this.durationTextBox;
            this.durationTextBox.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.durationTextBox.ForeColor = System.Drawing.Color.Black;
            this.durationTextBox.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.durationTextBox.HoverState.Parent = this.durationTextBox;
            this.durationTextBox.Location = new System.Drawing.Point(50, 60);
            this.durationTextBox.Name = "durationTextBox";
            this.durationTextBox.PasswordChar = '\0';
            this.durationTextBox.PlaceholderText = "";
            this.durationTextBox.SelectedText = "";
            this.durationTextBox.ShadowDecoration.Parent = this.durationTextBox;
            this.durationTextBox.Size = new System.Drawing.Size(205, 36);
            this.durationTextBox.TabIndex = 2;
            // 
            // labelTotalPrice
            // 
            this.labelTotalPrice.AutoSize = true;
            this.labelTotalPrice.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelTotalPrice.Location = new System.Drawing.Point(49, 482);
            this.labelTotalPrice.Name = "labelTotalPrice";
            this.labelTotalPrice.Size = new System.Drawing.Size(230, 19);
            this.labelTotalPrice.TabIndex = 3;
            this.labelTotalPrice.Text = "Вартість розмови складає:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(332, 549);
            this.Controls.Add(this.labelTotalPrice);
            this.Controls.Add(this.guna2Panel1);
            this.Controls.Add(this.exitBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.guna2Panel1.ResumeLayout(false);
            this.guna2Panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI2.WinForms.Guna2Button exitBtn;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel1;
        private System.Windows.Forms.Label label2;
        private Guna.UI2.WinForms.Guna2TextBox durationTextBox;
        private System.Windows.Forms.Label label3;
        private Guna.UI2.WinForms.Guna2TextBox numberOfGraceMinutesTextBox;
        private System.Windows.Forms.Label label4;
        private Guna.UI2.WinForms.Guna2TextBox preferentialTariffTextBox;
        private Guna.UI2.WinForms.Guna2TextBox tariffTextBox;
        private System.Windows.Forms.Label labelTotalPrice;
        private Guna.UI2.WinForms.Guna2Button calculateBtn;
    }
}

