﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IZVP2_Krylov
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void calculateBtn_Click(object sender, EventArgs e)
        {
            double totalPrice = 0;

            double duration = Convert.ToDouble(durationTextBox.Text);
            int numberOfGraceMinutes = Convert.ToInt32(numberOfGraceMinutesTextBox.Text);
            double preferentialTariff = Convert.ToDouble(preferentialTariffTextBox.Text);
            double tariff = Convert.ToDouble(tariffTextBox.Text);

            totalPrice = ((duration - numberOfGraceMinutes) * tariff) + (numberOfGraceMinutes * preferentialTariff);

            labelTotalPrice.Text = "Вартість розмови складає: " + totalPrice;
        }
    }
}
