﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChineseGame
{
    class Game
    {
        private uint sticksNumber;
        private GameState gameState;
        private bool isPersonWin = true;
        private bool isComputerWin = true;
        public delegate void GameStateHandler(object sender, GameStateArgs e);
        public event GameStateHandler takeSticks;

        public Game(uint sticksNumber)
        {
            this.sticksNumber = sticksNumber;

            gameState = GameState.HASNOTBEGUN;
        }

        public bool getSticksByPerson(uint sticksRequiredQuantity)
        {
            if (sticksRequiredQuantity > sticksNumber || sticksRequiredQuantity < 1 || sticksRequiredQuantity > 3)
            {
                Console.WriteLine("Please, input the value from 1 to 3 and less or equals than " + sticksNumber);
                return false;
            }

            sticksNumber -= sticksRequiredQuantity;

            if (sticksNumber == 0)
            {
                isPersonWin = false;
                gameState = GameState.FINISHED;
            }

            if(takeSticks != null)
            {
                takeSticks(this, new GameStateArgs($"The user took {sticksRequiredQuantity} sticks", sticksRequiredQuantity, sticksNumber));
            }

            return true;
        }

        public bool getSticksByComputer()
        {
            uint sticksRequiredQuantity = 0;

            int minValue = 1;
            int maxValue = 4;

            Random random = new Random();

            while (sticksRequiredQuantity == 0 || sticksRequiredQuantity > sticksNumber)
            {
                sticksRequiredQuantity = (uint) random.Next(minValue, maxValue);
            }

            sticksNumber -= sticksRequiredQuantity;

            if (sticksNumber == 0)
            {
                isComputerWin = false;
                gameState = GameState.FINISHED;
            }

            if (takeSticks != null)
            {
                takeSticks(this,new GameStateArgs($"A computer took {sticksRequiredQuantity} sticks", sticksRequiredQuantity, sticksNumber));
            }

            return true;
        }

        public void startGame()
        {
            gameState = GameState.INPROGRESS;

            this.takeSticks += showMessage;

            while (gameState == GameState.INPROGRESS)
            {
                Console.WriteLine(" Input the value of sticks => ");

                uint sticks = UInt16.Parse(Console.ReadLine());

                if(getSticksByPerson(sticks) && sticksNumber != 0)
                    getSticksByComputer();

                Console.WriteLine("Leftover sticks: " + sticksNumber);
            }

            if(isPersonWin)
            {
                Console.WriteLine("User is winner!");
            }
            else
            {
                Console.WriteLine("Computer is winner!");
            }
        }

        private static void showMessage(object sender, GameStateArgs e)
        {
            Console.WriteLine(e.message);
        }
    }

    enum GameState
    {
        HASNOTBEGUN, INPROGRESS, FINISHED
    }
}
