﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChineseGame
{
    class GameStateArgs
    {
        public string message { get; }
        public uint sticksNumber;
        public uint leftoverSticks;

        public GameStateArgs(string message, uint sticksNumber, uint leftoverSticks)
        {
            this.message = message;
            this.sticksNumber = sticksNumber;
        }
    }
}
