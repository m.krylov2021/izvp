﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RealEstate
{
    class Apartment :  RealEstate, CalculatingCostPerSquareMeter
    {
        private uint maxNumberOfInhabitants;

        Apartment(double price, String estateAddress,
                  uint square, DateTime changeDate,
                  State propertyStatus, Owner owner,
                  uint maxNumberOfInhabitants)
                  : base(price, estateAddress,
                        square, changeDate,
                        propertyStatus, owner)
        {
            this.maxNumberOfInhabitants = maxNumberOfInhabitants;
        }

        public double calculatingCostPerSquareMeter()
        {
            return getPrice() / getSquare();
        }
    }
}
