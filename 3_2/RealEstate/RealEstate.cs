﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RealEstate
{
    class RealEstate
    {
        private double price;
        private String estateAddress;
        private uint square;
        private DateTime changeDate;
        private State propertyStatus;
        private Owner owner;

        public RealEstate(double price, String estateAddress,
                          uint square, DateTime changeDate,
                          State propertyStatus, Owner owner)
        {
            this.price = price;
            this.estateAddress = estateAddress;
            this.square = square;
            this.changeDate = changeDate;
            this.propertyStatus = propertyStatus;
            this.owner = owner;
        }

        public double getPrice()
        {
            return price;
        }

        public uint getSquare()
        {
            return square;
        }
    }

    enum State
    {
        Rented,
        NotRented
    }
}
