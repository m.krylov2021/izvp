﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RealEstate
{
    class CommercialRealEstate : RealEstate, CalculatingCostPerSquareMeter
    {
        private String destination;

        public CommercialRealEstate(double price, String estateAddress,
                  uint square, DateTime changeDate,
                  State propertyStatus, Owner owner,
                  String destination)
                  : base(price, estateAddress,
                        square, changeDate,
                        propertyStatus, owner)
        {
            this.destination = destination;
        }

        public double calculatingCostPerSquareMeter()
        {
            return getPrice() / getSquare();
        }
    }
}
