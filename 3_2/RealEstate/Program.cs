﻿using System;

namespace RealEstate
{
    class Program
    {
        static void Main(string[] args)
        {
            var commercialRealEstate = new CommercialRealEstate(
                24454.24, "address", 435, DateTime.Now, State.NotRented, new Owner(), "destination");

            Console.WriteLine(commercialRealEstate.calculatingCostPerSquareMeter());
        }
    }
}
