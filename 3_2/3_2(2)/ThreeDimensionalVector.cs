﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _3_2_2_
{
    class ThreeDimensionalVector : Vector, ThreeDimensionalVectorHandling
    {
        private double x;
        private double y;
        private double z;

        public ThreeDimensionalVector(double x, double y, double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public double[] calculateScalarProduct(ThreeDimensionalVector multipliedVector)
        {
            double[] scalarProduct = new double[3];

            scalarProduct[0] = x * multipliedVector.getX();
            scalarProduct[1] = y * multipliedVector.getY();
            scalarProduct[2] = z * multipliedVector.getZ();

            return scalarProduct;
        }

        public double calculateVectorLength()
        {
            return Math.Sqrt(x*x + y*y + z*z);
        }

        public double[] calculateVectorSum(ThreeDimensionalVector additionVector)
        {
            double[] sum = new double[3];

            sum[0] = x + additionVector.getX();
            sum[1] = y + additionVector.getY();
            sum[2] = y + additionVector.getZ();

            return sum;
        }

        public double[] multiplyVectorByNumber(double number)
        {
            double[] multipliedVector = new double[3];

            multipliedVector[0] = number * getX();
            multipliedVector[1] = number * getY();
            multipliedVector[2] = number * getZ();

            return multipliedVector;
        }

        public double getX()
        {
            return x;
        }

        public double getY()
        {
            return y;
        }

        public double getZ()
        {
            return z;
        }
    }
}
