﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _3_2_2_
{
    class TwoDimensionalVector : Vector, TwoDimensionalVectorHandling
    {
        private double x;
        private double y;

        public TwoDimensionalVector(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        public double[] calculateScalarProduct(TwoDimensionalVector multipliedVector)
        {
            double[] scalarProduct = new double[2];

            scalarProduct[0] = x * multipliedVector.getX();
            scalarProduct[1] = y * multipliedVector.getY();

            return scalarProduct;
        }

        public double calculateVectorLength()
        {
            return Math.Sqrt(x*x + y*y);
        }

        public double[] calculateVectorSum(TwoDimensionalVector additionVector)
        {
            double[] sum = new double[2];

            sum[0] = x + additionVector.getX();
            sum[1] = y + additionVector.getY();

            return sum;
        }

        public double[] multiplyVectorByNumber(double number)
        {
            double[] multipliedVector = new double[2];

            multipliedVector[0] = number * getX();
            multipliedVector[1] = number * getY();

            return multipliedVector;
        }

        public double getX()
        {
            return x;
        }

        public double getY()
        {
            return y;
        }
    }
}
