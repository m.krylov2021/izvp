﻿using System;

namespace _3_2_2_
{
    class Program
    {
        static void Main(string[] args)
        {
            var twoDimensionalVector = new TwoDimensionalVector(1.4, 24.15);

            Console.WriteLine(twoDimensionalVector.calculateVectorLength());
        }
    }
}
