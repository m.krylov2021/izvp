﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _3_2_2_
{
    interface ThreeDimensionalVectorHandling
    {
        double calculateVectorLength();
        double[] calculateVectorSum(ThreeDimensionalVector additionVector);
        double[] multiplyVectorByNumber(double number);
        double[] calculateScalarProduct(ThreeDimensionalVector multipliedVector);
    }
}
