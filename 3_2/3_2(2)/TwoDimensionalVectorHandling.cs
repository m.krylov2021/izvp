﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _3_2_2_
{
    interface TwoDimensionalVectorHandling
    {
        double calculateVectorLength();
        double[] calculateVectorSum(TwoDimensionalVector additionVector);
        double[] multiplyVectorByNumber(double number);
        double[] calculateScalarProduct(TwoDimensionalVector multipliedVector);
    }
}
