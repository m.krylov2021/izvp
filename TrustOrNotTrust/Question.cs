﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrustOrNotTrust
{
    class Question
    {
        private string questionText;
        private bool answer;

        public string getQuestionText()
        {
            return questionText;
        }

        public bool getAnswer()
        {
            return answer;
        }

        public Question(string questionText, bool answer)
        {
            this.questionText = questionText;
            this.answer = answer;
        }
    }
}
