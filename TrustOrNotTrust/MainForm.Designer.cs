﻿
namespace TrustOrNotTrust
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.label1 = new System.Windows.Forms.Label();
            this.btnBelieve = new Guna.UI2.WinForms.Guna2Button();
            this.btnNotBelieve = new Guna.UI2.WinForms.Guna2Button();
            this.questionText = new System.Windows.Forms.Label();
            this.btnExit = new Guna.UI2.WinForms.Guna2Button();
            this.btnNext = new Guna.UI2.WinForms.Guna2Button();
            this.lbCorrectAnswers = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbWrongAnswers = new System.Windows.Forms.Label();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(199, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Питання:";
            // 
            // btnBelieve
            // 
            this.btnBelieve.Animated = true;
            this.btnBelieve.BorderRadius = 16;
            this.btnBelieve.CheckedState.Parent = this.btnBelieve;
            this.btnBelieve.CustomImages.Parent = this.btnBelieve;
            this.btnBelieve.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnBelieve.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnBelieve.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnBelieve.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnBelieve.DisabledState.Parent = this.btnBelieve;
            this.btnBelieve.FillColor = System.Drawing.Color.Green;
            this.btnBelieve.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnBelieve.ForeColor = System.Drawing.Color.White;
            this.btnBelieve.HoverState.Parent = this.btnBelieve;
            this.btnBelieve.Location = new System.Drawing.Point(145, 328);
            this.btnBelieve.Name = "btnBelieve";
            this.btnBelieve.ShadowDecoration.Parent = this.btnBelieve;
            this.btnBelieve.Size = new System.Drawing.Size(180, 45);
            this.btnBelieve.TabIndex = 1;
            this.btnBelieve.Text = "Вірю";
            this.btnBelieve.Click += new System.EventHandler(this.btnBelieve_Click);
            // 
            // btnNotBelieve
            // 
            this.btnNotBelieve.Animated = true;
            this.btnNotBelieve.BorderRadius = 16;
            this.btnNotBelieve.CheckedState.Parent = this.btnNotBelieve;
            this.btnNotBelieve.CustomImages.Parent = this.btnNotBelieve;
            this.btnNotBelieve.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnNotBelieve.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnNotBelieve.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnNotBelieve.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnNotBelieve.DisabledState.Parent = this.btnNotBelieve;
            this.btnNotBelieve.FillColor = System.Drawing.Color.Firebrick;
            this.btnNotBelieve.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnNotBelieve.ForeColor = System.Drawing.Color.White;
            this.btnNotBelieve.HoverState.Parent = this.btnNotBelieve;
            this.btnNotBelieve.Location = new System.Drawing.Point(145, 397);
            this.btnNotBelieve.Name = "btnNotBelieve";
            this.btnNotBelieve.ShadowDecoration.Parent = this.btnNotBelieve;
            this.btnNotBelieve.Size = new System.Drawing.Size(180, 45);
            this.btnNotBelieve.TabIndex = 2;
            this.btnNotBelieve.Text = "Не вірю";
            this.btnNotBelieve.Click += new System.EventHandler(this.btnNotBelieve_Click);
            // 
            // questionText
            // 
            this.questionText.AutoSize = true;
            this.questionText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.questionText.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.questionText.Location = new System.Drawing.Point(3, 0);
            this.questionText.Name = "questionText";
            this.questionText.Size = new System.Drawing.Size(117, 23);
            this.questionText.TabIndex = 3;
            this.questionText.Text = "questionText";
            // 
            // btnExit
            // 
            this.btnExit.BorderRadius = 8;
            this.btnExit.CheckedState.Parent = this.btnExit;
            this.btnExit.CustomImages.Parent = this.btnExit;
            this.btnExit.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnExit.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnExit.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnExit.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnExit.DisabledState.Parent = this.btnExit;
            this.btnExit.FillColor = System.Drawing.Color.Silver;
            this.btnExit.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.HoverState.Parent = this.btnExit;
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.Location = new System.Drawing.Point(450, 8);
            this.btnExit.Name = "btnExit";
            this.btnExit.ShadowDecoration.Parent = this.btnExit;
            this.btnExit.Size = new System.Drawing.Size(34, 35);
            this.btnExit.TabIndex = 4;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnNext
            // 
            this.btnNext.Animated = true;
            this.btnNext.BorderRadius = 16;
            this.btnNext.CheckedState.Parent = this.btnNext;
            this.btnNext.CustomImages.Parent = this.btnNext;
            this.btnNext.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnNext.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnNext.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnNext.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnNext.DisabledState.Parent = this.btnNext;
            this.btnNext.FillColor = System.Drawing.Color.DarkGoldenrod;
            this.btnNext.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext.ForeColor = System.Drawing.Color.White;
            this.btnNext.HoverState.Parent = this.btnNext;
            this.btnNext.Location = new System.Drawing.Point(145, 359);
            this.btnNext.Name = "btnNext";
            this.btnNext.ShadowDecoration.Parent = this.btnNext;
            this.btnNext.Size = new System.Drawing.Size(180, 45);
            this.btnNext.TabIndex = 5;
            this.btnNext.Text = "Далі";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // lbCorrectAnswers
            // 
            this.lbCorrectAnswers.AutoSize = true;
            this.lbCorrectAnswers.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbCorrectAnswers.Location = new System.Drawing.Point(268, 7);
            this.lbCorrectAnswers.Name = "lbCorrectAnswers";
            this.lbCorrectAnswers.Size = new System.Drawing.Size(134, 23);
            this.lbCorrectAnswers.TabIndex = 6;
            this.lbCorrectAnswers.Text = "Correct answers";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.questionText);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(27, 104);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(437, 210);
            this.flowLayoutPanel1.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(238, 20);
            this.label2.TabIndex = 8;
            this.label2.Text = "Кіл-ть правильних відповідей:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(12, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(256, 20);
            this.label3.TabIndex = 10;
            this.label3.Text = "Кіл-ть неправильних відповідей:";
            // 
            // lbWrongAnswers
            // 
            this.lbWrongAnswers.AutoSize = true;
            this.lbWrongAnswers.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbWrongAnswers.Location = new System.Drawing.Point(268, 33);
            this.lbWrongAnswers.Name = "lbWrongAnswers";
            this.lbWrongAnswers.Size = new System.Drawing.Size(129, 23);
            this.lbWrongAnswers.TabIndex = 9;
            this.lbWrongAnswers.Text = "Wrong answers";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(491, 448);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lbWrongAnswers);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.lbCorrectAnswers);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnNotBelieve);
            this.Controls.Add(this.btnBelieve);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2Button btnBelieve;
        private Guna.UI2.WinForms.Guna2Button btnNotBelieve;
        private System.Windows.Forms.Label questionText;
        private Guna.UI2.WinForms.Guna2Button btnExit;
        private Guna.UI2.WinForms.Guna2Button btnNext;
        private System.Windows.Forms.Label lbCorrectAnswers;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbWrongAnswers;
    }
}

