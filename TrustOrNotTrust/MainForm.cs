﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrustOrNotTrust
{
    public partial class MainForm : Form
    {
        uint correctAnswers = 0;
        uint wrongAnswers = 0;

        Queue<Question> questionsQueue = new Queue<Question>();
        Question currentQuestion;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            btnNext.Visible = false;
            updateCounters();

            questionsQueue.Enqueue(new Question("В сутках 24 часа.", true));
            questionsQueue.Enqueue(new Question("Небо зеленого цвета", false));
            questionsQueue.Enqueue(new Question("3", false));
            questionsQueue.Enqueue(new Question("4", true));
            questionsQueue.Enqueue(new Question("5", false));

            questionText.Text = questionsQueue.Peek().getQuestionText();
        }

        private void btnBelieve_Click(object sender, EventArgs e)
        {
            currentQuestion = questionsQueue.Dequeue();
            
            if(currentQuestion.getAnswer())
            {
                correctAnswers++;
            }
            else
            {
                wrongAnswers++;
            }

            updateCounters();

            checkNumbersOfWrongNumbers();

            hideButtons();
            btnNext.Visible = true;
        }

        private void btnNotBelieve_Click(object sender, EventArgs e)
        {
            currentQuestion = questionsQueue.Dequeue();

            if (!currentQuestion.getAnswer())
            {
                correctAnswers++;
            }
            else
            {
                wrongAnswers++;
            }

            updateCounters();

            checkNumbersOfWrongNumbers();

            hideButtons();
            btnNext.Visible = true;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if(questionsQueue.Count == 0)
            {
                MessageBox.Show("Дякуємо! Ви перемогли! Гру завершено.", "Інформація");
                Application.Exit();
            }
            else
            {
                questionText.Text = questionsQueue.Peek().getQuestionText();
            }

            showButtons();
            btnNext.Visible = false;

        }

        public void hideButtons()
        {
            btnBelieve.Visible = false;
            btnNotBelieve.Visible = false;
        }

        public void showButtons()
        {
            btnBelieve.Visible = true;
            btnNotBelieve.Visible = true;
        }

        public void checkNumbersOfWrongNumbers()
        {
            if(wrongAnswers == 2)
            {
                MessageBox.Show("Спробуйте ще(", "Гру завершено");
                Application.Exit();
            }
        }

        public void updateCounters()
        {
            lbCorrectAnswers.Text = correctAnswers.ToString();
            lbWrongAnswers.Text = wrongAnswers.ToString();
        }
    }
}
