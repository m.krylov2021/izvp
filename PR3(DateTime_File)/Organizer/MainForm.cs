﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Organizer
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if(rtbTask.Text != "")
            {
                dgv.Rows.Add(DateTime.Now.ToShortDateString(), dtpSelectedDate.Value.ToShortDateString(),
                    rtbTask.Text, "Не виконано");
            }
            else
            {
                MessageBox.Show("Заповніть усі поля!");
            }
        }

        int rowIndex = -1;
        private void dgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            rowIndex = e.RowIndex;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if(rowIndex != -1)
            {
                try
                {
                    dgv.Rows.RemoveAt(rowIndex);
                    dgv.Refresh();
                }
                catch (Exception exception)
                {
                    MessageBox.Show("Помилка!" + exception);
                }
            }
            else
            {
                MessageBox.Show("Оберіть запис!");
            }
            
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {

        }
    }
}
