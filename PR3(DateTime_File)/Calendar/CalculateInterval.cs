﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class CalculateInterval : Form
    {

        public CalculateInterval()
        {
            InitializeComponent();
        }

        private void backToMainMenu_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void calculateBtn_Click(object sender, EventArgs e)
        {
            DateTime date1 = dtp1.Value;
            DateTime date2 = dtp2.Value;
            
            TimeSpan dur;

            dur =  date1 > date2 ?  date1 - date2 : date2 - date1;

            yearsTextBox.Text = Convert.ToString((int) dur.TotalDays / 365);
            monthsTextBox.Text = Convert.ToString((int) dur.TotalDays / 30);
            daysTextBox.Text = Convert.ToString((int) dur.TotalDays);
            hoursTextBox.Text = Convert.ToString((int) dur.TotalHours);
            minutesTextBox.Text = Convert.ToString((int) dur.TotalMinutes);
        }
    }
}
