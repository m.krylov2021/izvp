﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class MainForm : Form
    {
        List<String> dates = new List<string>() { "31.12", "08.03", "14.10" };

        public MainForm()
        {
            InitializeComponent();
        }

        private void checkDate()
        {
            String ourDate = monthCalendar1.SelectionStart.ToShortDateString();

            labelSelectedDate.Text = "Ваша дата: " + ourDate;
            labelSelectedDate.ForeColor = Color.Black;

            if (monthCalendar1.SelectionStart.DayOfWeek == DayOfWeek.Sunday ||
                monthCalendar1.SelectionStart.DayOfWeek == DayOfWeek.Saturday)
            {
                labelSelectedDate.ForeColor = Color.Red;
            }
            else
            {
                for (int i = 0; i < dates.Count; i++)
                {
                    if (ourDate.Contains(dates[i]))
                    {
                        labelSelectedDate.ForeColor = Color.Red;
                    }
                }
            }
        }

        private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
            checkDate();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            //labelSelectedDate.Text = "Today is " + DateTime.Today.ToShortDateString();
            checkDate();
        }

        private void intervalBetweenDates_Click(object sender, EventArgs e)
        {
            CalculateInterval form = new CalculateInterval();
            form.Show();
        }

        private void changeDate_Click(object sender, EventArgs e)
        {
            DateChange form = new DateChange();
            form.Show();
        }

        private void temperatureAndPressure_Click(object sender, EventArgs e)
        {
            Temperature form = new Temperature();
            form.Show();
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
