﻿
namespace WindowsFormsApp1
{
    partial class DateChange
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.backToMainMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.labelBeginTime = new System.Windows.Forms.Label();
            this.dtpBeginDate = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.grpb = new System.Windows.Forms.GroupBox();
            this.rdbYears = new Guna.UI2.WinForms.Guna2RadioButton();
            this.rdbMonth = new Guna.UI2.WinForms.Guna2RadioButton();
            this.rdbWeeks = new Guna.UI2.WinForms.Guna2RadioButton();
            this.rdbDays = new Guna.UI2.WinForms.Guna2RadioButton();
            this.rdbMinutes = new Guna.UI2.WinForms.Guna2RadioButton();
            this.rdbHours = new Guna.UI2.WinForms.Guna2RadioButton();
            this.rdbSeconds = new Guna.UI2.WinForms.Guna2RadioButton();
            this.labelValue = new System.Windows.Forms.Label();
            this.tbValue = new Guna.UI2.WinForms.Guna2TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpResultDate = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.btnChangeDate = new Guna.UI2.WinForms.Guna2Button();
            this.menuStrip1.SuspendLayout();
            this.grpb.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.backToMainMenu});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(271, 29);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // backToMainMenu
            // 
            this.backToMainMenu.Name = "backToMainMenu";
            this.backToMainMenu.Size = new System.Drawing.Size(259, 25);
            this.backToMainMenu.Text = "Повернутись до головного меню";
            this.backToMainMenu.Click += new System.EventHandler(this.backToMainMenu_Click);
            // 
            // labelBeginTime
            // 
            this.labelBeginTime.AutoSize = true;
            this.labelBeginTime.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelBeginTime.Location = new System.Drawing.Point(57, 46);
            this.labelBeginTime.Name = "labelBeginTime";
            this.labelBeginTime.Size = new System.Drawing.Size(153, 38);
            this.labelBeginTime.TabIndex = 9;
            this.labelBeginTime.Text = "Введіть початкове\r\nзначення дати/часу";
            // 
            // dtpBeginDate
            // 
            this.dtpBeginDate.CheckedState.Parent = this.dtpBeginDate;
            this.dtpBeginDate.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.dtpBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.dtpBeginDate.HoverState.Parent = this.dtpBeginDate;
            this.dtpBeginDate.Location = new System.Drawing.Point(34, 94);
            this.dtpBeginDate.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.dtpBeginDate.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtpBeginDate.Name = "dtpBeginDate";
            this.dtpBeginDate.ShadowDecoration.Parent = this.dtpBeginDate;
            this.dtpBeginDate.Size = new System.Drawing.Size(200, 36);
            this.dtpBeginDate.TabIndex = 10;
            this.dtpBeginDate.Value = new System.DateTime(2021, 10, 16, 13, 31, 39, 692);
            // 
            // grpb
            // 
            this.grpb.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.grpb.Controls.Add(this.rdbSeconds);
            this.grpb.Controls.Add(this.rdbHours);
            this.grpb.Controls.Add(this.rdbDays);
            this.grpb.Controls.Add(this.rdbWeeks);
            this.grpb.Controls.Add(this.rdbMinutes);
            this.grpb.Controls.Add(this.rdbMonth);
            this.grpb.Controls.Add(this.rdbYears);
            this.grpb.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.grpb.Location = new System.Drawing.Point(45, 146);
            this.grpb.Name = "grpb";
            this.grpb.Size = new System.Drawing.Size(176, 237);
            this.grpb.TabIndex = 11;
            this.grpb.TabStop = false;
            this.grpb.Text = "Змінити дату на";
            // 
            // rdbYears
            // 
            this.rdbYears.AutoSize = true;
            this.rdbYears.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.rdbYears.CheckedState.BorderThickness = 0;
            this.rdbYears.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.rdbYears.CheckedState.InnerColor = System.Drawing.Color.White;
            this.rdbYears.CheckedState.InnerOffset = -4;
            this.rdbYears.Location = new System.Drawing.Point(18, 30);
            this.rdbYears.Name = "rdbYears";
            this.rdbYears.Size = new System.Drawing.Size(62, 23);
            this.rdbYears.TabIndex = 0;
            this.rdbYears.Text = "роки";
            this.rdbYears.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.rdbYears.UncheckedState.BorderThickness = 2;
            this.rdbYears.UncheckedState.FillColor = System.Drawing.Color.Transparent;
            this.rdbYears.UncheckedState.InnerColor = System.Drawing.Color.Transparent;
            // 
            // rdbMonth
            // 
            this.rdbMonth.AutoSize = true;
            this.rdbMonth.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.rdbMonth.CheckedState.BorderThickness = 0;
            this.rdbMonth.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.rdbMonth.CheckedState.InnerColor = System.Drawing.Color.White;
            this.rdbMonth.CheckedState.InnerOffset = -4;
            this.rdbMonth.Location = new System.Drawing.Point(18, 59);
            this.rdbMonth.Name = "rdbMonth";
            this.rdbMonth.Size = new System.Drawing.Size(69, 23);
            this.rdbMonth.TabIndex = 1;
            this.rdbMonth.Text = "місяці";
            this.rdbMonth.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.rdbMonth.UncheckedState.BorderThickness = 2;
            this.rdbMonth.UncheckedState.FillColor = System.Drawing.Color.Transparent;
            this.rdbMonth.UncheckedState.InnerColor = System.Drawing.Color.Transparent;
            // 
            // rdbWeeks
            // 
            this.rdbWeeks.AutoSize = true;
            this.rdbWeeks.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.rdbWeeks.CheckedState.BorderThickness = 0;
            this.rdbWeeks.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.rdbWeeks.CheckedState.InnerColor = System.Drawing.Color.White;
            this.rdbWeeks.CheckedState.InnerOffset = -4;
            this.rdbWeeks.Location = new System.Drawing.Point(18, 88);
            this.rdbWeeks.Name = "rdbWeeks";
            this.rdbWeeks.Size = new System.Drawing.Size(69, 23);
            this.rdbWeeks.TabIndex = 2;
            this.rdbWeeks.Text = "тижні";
            this.rdbWeeks.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.rdbWeeks.UncheckedState.BorderThickness = 2;
            this.rdbWeeks.UncheckedState.FillColor = System.Drawing.Color.Transparent;
            this.rdbWeeks.UncheckedState.InnerColor = System.Drawing.Color.Transparent;
            // 
            // rdbDays
            // 
            this.rdbDays.AutoSize = true;
            this.rdbDays.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.rdbDays.CheckedState.BorderThickness = 0;
            this.rdbDays.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.rdbDays.CheckedState.InnerColor = System.Drawing.Color.White;
            this.rdbDays.CheckedState.InnerOffset = -4;
            this.rdbDays.Location = new System.Drawing.Point(18, 117);
            this.rdbDays.Name = "rdbDays";
            this.rdbDays.Size = new System.Drawing.Size(49, 23);
            this.rdbDays.TabIndex = 3;
            this.rdbDays.Text = "дні";
            this.rdbDays.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.rdbDays.UncheckedState.BorderThickness = 2;
            this.rdbDays.UncheckedState.FillColor = System.Drawing.Color.Transparent;
            this.rdbDays.UncheckedState.InnerColor = System.Drawing.Color.Transparent;
            // 
            // rdbMinutes
            // 
            this.rdbMinutes.AutoSize = true;
            this.rdbMinutes.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.rdbMinutes.CheckedState.BorderThickness = 0;
            this.rdbMinutes.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.rdbMinutes.CheckedState.InnerColor = System.Drawing.Color.White;
            this.rdbMinutes.CheckedState.InnerOffset = -4;
            this.rdbMinutes.Location = new System.Drawing.Point(18, 175);
            this.rdbMinutes.Name = "rdbMinutes";
            this.rdbMinutes.Size = new System.Drawing.Size(88, 23);
            this.rdbMinutes.TabIndex = 4;
            this.rdbMinutes.Text = "хвилини";
            this.rdbMinutes.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.rdbMinutes.UncheckedState.BorderThickness = 2;
            this.rdbMinutes.UncheckedState.FillColor = System.Drawing.Color.Transparent;
            this.rdbMinutes.UncheckedState.InnerColor = System.Drawing.Color.Transparent;
            // 
            // rdbHours
            // 
            this.rdbHours.AutoSize = true;
            this.rdbHours.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.rdbHours.CheckedState.BorderThickness = 0;
            this.rdbHours.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.rdbHours.CheckedState.InnerColor = System.Drawing.Color.White;
            this.rdbHours.CheckedState.InnerOffset = -4;
            this.rdbHours.Location = new System.Drawing.Point(18, 146);
            this.rdbHours.Name = "rdbHours";
            this.rdbHours.Size = new System.Drawing.Size(79, 23);
            this.rdbHours.TabIndex = 4;
            this.rdbHours.Text = "години";
            this.rdbHours.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.rdbHours.UncheckedState.BorderThickness = 2;
            this.rdbHours.UncheckedState.FillColor = System.Drawing.Color.Transparent;
            this.rdbHours.UncheckedState.InnerColor = System.Drawing.Color.Transparent;
            // 
            // rdbSeconds
            // 
            this.rdbSeconds.AutoSize = true;
            this.rdbSeconds.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.rdbSeconds.CheckedState.BorderThickness = 0;
            this.rdbSeconds.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.rdbSeconds.CheckedState.InnerColor = System.Drawing.Color.White;
            this.rdbSeconds.CheckedState.InnerOffset = -4;
            this.rdbSeconds.Location = new System.Drawing.Point(18, 205);
            this.rdbSeconds.Name = "rdbSeconds";
            this.rdbSeconds.Size = new System.Drawing.Size(85, 23);
            this.rdbSeconds.TabIndex = 5;
            this.rdbSeconds.Text = "секунди";
            this.rdbSeconds.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.rdbSeconds.UncheckedState.BorderThickness = 2;
            this.rdbSeconds.UncheckedState.FillColor = System.Drawing.Color.Transparent;
            this.rdbSeconds.UncheckedState.InnerColor = System.Drawing.Color.Transparent;
            // 
            // labelValue
            // 
            this.labelValue.AutoSize = true;
            this.labelValue.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelValue.Location = new System.Drawing.Point(34, 395);
            this.labelValue.Name = "labelValue";
            this.labelValue.Size = new System.Drawing.Size(129, 38);
            this.labelValue.TabIndex = 12;
            this.labelValue.Text = "На яке значення\r\nзмінювати";
            // 
            // tbValue
            // 
            this.tbValue.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbValue.DefaultText = "";
            this.tbValue.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.tbValue.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.tbValue.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.tbValue.DisabledState.Parent = this.tbValue;
            this.tbValue.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.tbValue.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.tbValue.FocusedState.Parent = this.tbValue;
            this.tbValue.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbValue.ForeColor = System.Drawing.Color.Black;
            this.tbValue.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.tbValue.HoverState.Parent = this.tbValue;
            this.tbValue.Location = new System.Drawing.Point(182, 398);
            this.tbValue.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.tbValue.Name = "tbValue";
            this.tbValue.PasswordChar = '\0';
            this.tbValue.PlaceholderText = "";
            this.tbValue.SelectedText = "";
            this.tbValue.ShadowDecoration.Parent = this.tbValue;
            this.tbValue.Size = new System.Drawing.Size(50, 38);
            this.tbValue.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(76, 512);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 19);
            this.label1.TabIndex = 14;
            this.label1.Text = "Нова дата/час";
            // 
            // dtpResultDate
            // 
            this.dtpResultDate.CheckedState.Parent = this.dtpResultDate;
            this.dtpResultDate.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.dtpResultDate.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.dtpResultDate.HoverState.Parent = this.dtpResultDate;
            this.dtpResultDate.Location = new System.Drawing.Point(34, 534);
            this.dtpResultDate.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.dtpResultDate.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtpResultDate.Name = "dtpResultDate";
            this.dtpResultDate.ShadowDecoration.Parent = this.dtpResultDate;
            this.dtpResultDate.Size = new System.Drawing.Size(200, 36);
            this.dtpResultDate.TabIndex = 15;
            this.dtpResultDate.Value = new System.DateTime(2021, 10, 16, 13, 38, 52, 681);
            // 
            // btnChangeDate
            // 
            this.btnChangeDate.Animated = true;
            this.btnChangeDate.BorderRadius = 14;
            this.btnChangeDate.CheckedState.Parent = this.btnChangeDate;
            this.btnChangeDate.CustomImages.Parent = this.btnChangeDate;
            this.btnChangeDate.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnChangeDate.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnChangeDate.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnChangeDate.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnChangeDate.DisabledState.Parent = this.btnChangeDate;
            this.btnChangeDate.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnChangeDate.ForeColor = System.Drawing.Color.White;
            this.btnChangeDate.HoverState.Parent = this.btnChangeDate;
            this.btnChangeDate.Location = new System.Drawing.Point(45, 448);
            this.btnChangeDate.Name = "btnChangeDate";
            this.btnChangeDate.ShadowDecoration.Parent = this.btnChangeDate;
            this.btnChangeDate.Size = new System.Drawing.Size(176, 45);
            this.btnChangeDate.TabIndex = 16;
            this.btnChangeDate.Text = "Змінити дату";
            this.btnChangeDate.Click += new System.EventHandler(this.btnChangeDate_Click);
            // 
            // DateChange
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(271, 592);
            this.Controls.Add(this.btnChangeDate);
            this.Controls.Add(this.dtpResultDate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbValue);
            this.Controls.Add(this.labelValue);
            this.Controls.Add(this.grpb);
            this.Controls.Add(this.dtpBeginDate);
            this.Controls.Add(this.labelBeginTime);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "DateChange";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DateChange";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.grpb.ResumeLayout(false);
            this.grpb.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem backToMainMenu;
        private System.Windows.Forms.Label labelBeginTime;
        private Guna.UI2.WinForms.Guna2DateTimePicker dtpBeginDate;
        private System.Windows.Forms.GroupBox grpb;
        private Guna.UI2.WinForms.Guna2RadioButton rdbSeconds;
        private Guna.UI2.WinForms.Guna2RadioButton rdbHours;
        private Guna.UI2.WinForms.Guna2RadioButton rdbMinutes;
        private Guna.UI2.WinForms.Guna2RadioButton rdbDays;
        private Guna.UI2.WinForms.Guna2RadioButton rdbWeeks;
        private Guna.UI2.WinForms.Guna2RadioButton rdbMonth;
        private Guna.UI2.WinForms.Guna2RadioButton rdbYears;
        private System.Windows.Forms.Label labelValue;
        private Guna.UI2.WinForms.Guna2TextBox tbValue;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2DateTimePicker dtpResultDate;
        private Guna.UI2.WinForms.Guna2Button btnChangeDate;
    }
}