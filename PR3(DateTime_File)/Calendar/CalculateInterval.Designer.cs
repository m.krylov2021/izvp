﻿
namespace WindowsFormsApp1
{
    partial class CalculateInterval
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtp1 = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.dtp2 = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.labelFirstDate = new System.Windows.Forms.Label();
            this.labelSecondDate = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.calculateBtn = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Panel1 = new Guna.UI2.WinForms.Guna2Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.daysTextBox = new Guna.UI2.WinForms.Guna2TextBox();
            this.minutesTextBox = new Guna.UI2.WinForms.Guna2TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.hoursTextBox = new Guna.UI2.WinForms.Guna2TextBox();
            this.monthsTextBox = new Guna.UI2.WinForms.Guna2TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.yearsTextBox = new Guna.UI2.WinForms.Guna2TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.backToMainMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.guna2Panel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dtp1
            // 
            this.dtp1.CheckedState.Parent = this.dtp1;
            this.dtp1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.dtp1.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.dtp1.HoverState.Parent = this.dtp1;
            this.dtp1.Location = new System.Drawing.Point(227, 45);
            this.dtp1.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.dtp1.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtp1.Name = "dtp1";
            this.dtp1.ShadowDecoration.Parent = this.dtp1;
            this.dtp1.Size = new System.Drawing.Size(236, 36);
            this.dtp1.TabIndex = 0;
            this.dtp1.Value = new System.DateTime(2021, 9, 24, 9, 29, 42, 713);
            // 
            // dtp2
            // 
            this.dtp2.CheckedState.Parent = this.dtp2;
            this.dtp2.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.dtp2.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.dtp2.HoverState.Parent = this.dtp2;
            this.dtp2.Location = new System.Drawing.Point(227, 93);
            this.dtp2.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.dtp2.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtp2.Name = "dtp2";
            this.dtp2.ShadowDecoration.Parent = this.dtp2;
            this.dtp2.Size = new System.Drawing.Size(236, 36);
            this.dtp2.TabIndex = 1;
            this.dtp2.Value = new System.DateTime(2021, 9, 24, 9, 29, 42, 713);
            // 
            // labelFirstDate
            // 
            this.labelFirstDate.AutoSize = true;
            this.labelFirstDate.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelFirstDate.Location = new System.Drawing.Point(52, 54);
            this.labelFirstDate.Name = "labelFirstDate";
            this.labelFirstDate.Size = new System.Drawing.Size(158, 21);
            this.labelFirstDate.TabIndex = 2;
            this.labelFirstDate.Text = "Введіть першу дату";
            // 
            // labelSecondDate
            // 
            this.labelSecondDate.AutoSize = true;
            this.labelSecondDate.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSecondDate.Location = new System.Drawing.Point(52, 101);
            this.labelSecondDate.Name = "labelSecondDate";
            this.labelSecondDate.Size = new System.Drawing.Size(146, 21);
            this.labelSecondDate.TabIndex = 3;
            this.labelSecondDate.Text = "Введіть другу дату";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(138, 165);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(211, 42);
            this.label3.TabIndex = 4;
            this.label3.Text = "Визначити проміжок часу\r\nпоміж цими датами";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // calculateBtn
            // 
            this.calculateBtn.BorderRadius = 12;
            this.calculateBtn.CheckedState.Parent = this.calculateBtn;
            this.calculateBtn.CustomImages.Parent = this.calculateBtn;
            this.calculateBtn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.calculateBtn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.calculateBtn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.calculateBtn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.calculateBtn.DisabledState.Parent = this.calculateBtn;
            this.calculateBtn.FillColor = System.Drawing.Color.DarkOrange;
            this.calculateBtn.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.calculateBtn.ForeColor = System.Drawing.Color.White;
            this.calculateBtn.HoverState.Parent = this.calculateBtn;
            this.calculateBtn.Location = new System.Drawing.Point(150, 218);
            this.calculateBtn.Name = "calculateBtn";
            this.calculateBtn.ShadowDecoration.Parent = this.calculateBtn;
            this.calculateBtn.Size = new System.Drawing.Size(180, 45);
            this.calculateBtn.TabIndex = 5;
            this.calculateBtn.Text = "ОК";
            this.calculateBtn.Click += new System.EventHandler(this.calculateBtn_Click);
            // 
            // guna2Panel1
            // 
            this.guna2Panel1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.guna2Panel1.Controls.Add(this.label6);
            this.guna2Panel1.Controls.Add(this.daysTextBox);
            this.guna2Panel1.Controls.Add(this.minutesTextBox);
            this.guna2Panel1.Controls.Add(this.label5);
            this.guna2Panel1.Controls.Add(this.hoursTextBox);
            this.guna2Panel1.Controls.Add(this.monthsTextBox);
            this.guna2Panel1.Controls.Add(this.label4);
            this.guna2Panel1.Controls.Add(this.label1);
            this.guna2Panel1.Controls.Add(this.label2);
            this.guna2Panel1.Controls.Add(this.yearsTextBox);
            this.guna2Panel1.Location = new System.Drawing.Point(31, 295);
            this.guna2Panel1.Name = "guna2Panel1";
            this.guna2Panel1.ShadowDecoration.Parent = this.guna2Panel1;
            this.guna2Panel1.Size = new System.Drawing.Size(432, 180);
            this.guna2Panel1.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(219, 109);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 16);
            this.label6.TabIndex = 12;
            this.label6.Text = "в хвилинах";
            // 
            // daysTextBox
            // 
            this.daysTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.daysTextBox.DefaultText = "";
            this.daysTextBox.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.daysTextBox.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.daysTextBox.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.daysTextBox.DisabledState.Parent = this.daysTextBox;
            this.daysTextBox.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.daysTextBox.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.daysTextBox.FocusedState.Parent = this.daysTextBox;
            this.daysTextBox.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.daysTextBox.ForeColor = System.Drawing.Color.Black;
            this.daysTextBox.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.daysTextBox.HoverState.Parent = this.daysTextBox;
            this.daysTextBox.Location = new System.Drawing.Point(93, 129);
            this.daysTextBox.Name = "daysTextBox";
            this.daysTextBox.PasswordChar = '\0';
            this.daysTextBox.PlaceholderText = "";
            this.daysTextBox.ReadOnly = true;
            this.daysTextBox.SelectedText = "";
            this.daysTextBox.ShadowDecoration.Parent = this.daysTextBox;
            this.daysTextBox.Size = new System.Drawing.Size(97, 31);
            this.daysTextBox.TabIndex = 4;
            // 
            // minutesTextBox
            // 
            this.minutesTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.minutesTextBox.DefaultText = "";
            this.minutesTextBox.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.minutesTextBox.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.minutesTextBox.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.minutesTextBox.DisabledState.Parent = this.minutesTextBox;
            this.minutesTextBox.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.minutesTextBox.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.minutesTextBox.FocusedState.Parent = this.minutesTextBox;
            this.minutesTextBox.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.minutesTextBox.ForeColor = System.Drawing.Color.Black;
            this.minutesTextBox.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.minutesTextBox.HoverState.Parent = this.minutesTextBox;
            this.minutesTextBox.Location = new System.Drawing.Point(307, 103);
            this.minutesTextBox.Name = "minutesTextBox";
            this.minutesTextBox.PasswordChar = '\0';
            this.minutesTextBox.PlaceholderText = "";
            this.minutesTextBox.ReadOnly = true;
            this.minutesTextBox.SelectedText = "";
            this.minutesTextBox.ShadowDecoration.Parent = this.minutesTextBox;
            this.minutesTextBox.Size = new System.Drawing.Size(97, 31);
            this.minutesTextBox.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(219, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 16);
            this.label5.TabIndex = 11;
            this.label5.Text = "в годинах";
            // 
            // hoursTextBox
            // 
            this.hoursTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.hoursTextBox.DefaultText = "";
            this.hoursTextBox.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.hoursTextBox.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.hoursTextBox.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.hoursTextBox.DisabledState.Parent = this.hoursTextBox;
            this.hoursTextBox.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.hoursTextBox.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.hoursTextBox.FocusedState.Parent = this.hoursTextBox;
            this.hoursTextBox.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.hoursTextBox.ForeColor = System.Drawing.Color.Black;
            this.hoursTextBox.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.hoursTextBox.HoverState.Parent = this.hoursTextBox;
            this.hoursTextBox.Location = new System.Drawing.Point(307, 45);
            this.hoursTextBox.Name = "hoursTextBox";
            this.hoursTextBox.PasswordChar = '\0';
            this.hoursTextBox.PlaceholderText = "";
            this.hoursTextBox.ReadOnly = true;
            this.hoursTextBox.SelectedText = "";
            this.hoursTextBox.ShadowDecoration.Parent = this.hoursTextBox;
            this.hoursTextBox.Size = new System.Drawing.Size(97, 31);
            this.hoursTextBox.TabIndex = 3;
            // 
            // monthsTextBox
            // 
            this.monthsTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.monthsTextBox.DefaultText = "";
            this.monthsTextBox.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.monthsTextBox.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.monthsTextBox.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.monthsTextBox.DisabledState.Parent = this.monthsTextBox;
            this.monthsTextBox.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.monthsTextBox.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.monthsTextBox.FocusedState.Parent = this.monthsTextBox;
            this.monthsTextBox.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.monthsTextBox.ForeColor = System.Drawing.Color.Black;
            this.monthsTextBox.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.monthsTextBox.HoverState.Parent = this.monthsTextBox;
            this.monthsTextBox.Location = new System.Drawing.Point(93, 77);
            this.monthsTextBox.Name = "monthsTextBox";
            this.monthsTextBox.PasswordChar = '\0';
            this.monthsTextBox.PlaceholderText = "";
            this.monthsTextBox.ReadOnly = true;
            this.monthsTextBox.SelectedText = "";
            this.monthsTextBox.ShadowDecoration.Parent = this.monthsTextBox;
            this.monthsTextBox.Size = new System.Drawing.Size(97, 31);
            this.monthsTextBox.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(22, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 16);
            this.label4.TabIndex = 10;
            this.label4.Text = "в днях";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(22, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 16);
            this.label1.TabIndex = 8;
            this.label1.Text = "в роках";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(22, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 16);
            this.label2.TabIndex = 9;
            this.label2.Text = "в місяцях";
            // 
            // yearsTextBox
            // 
            this.yearsTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.yearsTextBox.DefaultText = "";
            this.yearsTextBox.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.yearsTextBox.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.yearsTextBox.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.yearsTextBox.DisabledState.Parent = this.yearsTextBox;
            this.yearsTextBox.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.yearsTextBox.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.yearsTextBox.FocusedState.Parent = this.yearsTextBox;
            this.yearsTextBox.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.yearsTextBox.ForeColor = System.Drawing.Color.Black;
            this.yearsTextBox.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.yearsTextBox.HoverState.Parent = this.yearsTextBox;
            this.yearsTextBox.Location = new System.Drawing.Point(93, 23);
            this.yearsTextBox.Name = "yearsTextBox";
            this.yearsTextBox.PasswordChar = '\0';
            this.yearsTextBox.PlaceholderText = "";
            this.yearsTextBox.ReadOnly = true;
            this.yearsTextBox.SelectedText = "";
            this.yearsTextBox.ShadowDecoration.Parent = this.yearsTextBox;
            this.yearsTextBox.Size = new System.Drawing.Size(97, 31);
            this.yearsTextBox.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.backToMainMenu});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(494, 29);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // backToMainMenu
            // 
            this.backToMainMenu.Name = "backToMainMenu";
            this.backToMainMenu.Size = new System.Drawing.Size(259, 25);
            this.backToMainMenu.Text = "Повернутись до головного меню";
            this.backToMainMenu.Click += new System.EventHandler(this.backToMainMenu_Click);
            // 
            // CalculateInterval
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(494, 495);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.guna2Panel1);
            this.Controls.Add(this.calculateBtn);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.labelSecondDate);
            this.Controls.Add(this.labelFirstDate);
            this.Controls.Add(this.dtp2);
            this.Controls.Add(this.dtp1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "CalculateInterval";
            this.Text = "CalculateInterval";
            this.guna2Panel1.ResumeLayout(false);
            this.guna2Panel1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI2.WinForms.Guna2DateTimePicker dtp1;
        private Guna.UI2.WinForms.Guna2DateTimePicker dtp2;
        private System.Windows.Forms.Label labelFirstDate;
        private System.Windows.Forms.Label labelSecondDate;
        private System.Windows.Forms.Label label3;
        private Guna.UI2.WinForms.Guna2Button calculateBtn;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem backToMainMenu;
        private Guna.UI2.WinForms.Guna2TextBox daysTextBox;
        private Guna.UI2.WinForms.Guna2TextBox hoursTextBox;
        private Guna.UI2.WinForms.Guna2TextBox minutesTextBox;
        private Guna.UI2.WinForms.Guna2TextBox monthsTextBox;
        private Guna.UI2.WinForms.Guna2TextBox yearsTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}