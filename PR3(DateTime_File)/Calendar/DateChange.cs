﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class DateChange : Form
    {
        public DateChange()
        {
            InitializeComponent();
        }

        private void backToMainMenu_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnChangeDate_Click(object sender, EventArgs e)
        {
            if(tbValue.Text != "")
            {
                int value = Int32.Parse(tbValue.Text);

                if (rdbYears.Checked) dtpResultDate.Value = dtpBeginDate.Value.AddYears(value);
                if (rdbMonth.Checked) dtpResultDate.Value = dtpBeginDate.Value.AddMonths(value);
                if (rdbWeeks.Checked) dtpResultDate.Value = dtpBeginDate.Value.AddDays(value * 7);
                if (rdbDays.Checked) dtpResultDate.Value = dtpBeginDate.Value.AddDays(value);
                if (rdbHours.Checked) dtpResultDate.Value = dtpBeginDate.Value.AddHours(value);
                if (rdbMinutes.Checked) dtpResultDate.Value = dtpBeginDate.Value.AddMinutes(value);
                if (rdbSeconds.Checked) dtpResultDate.Value = dtpBeginDate.Value.AddSeconds(value);
            }
            else { MessageBox.Show("Введіть значення!"); }
            
        }
    }
}
