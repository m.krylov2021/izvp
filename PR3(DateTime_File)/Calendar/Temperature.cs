﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Temperature : Form
    {
        public Temperature()
        {
            InitializeComponent();
        }

        private void backToMainMenu_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddInformation_Click(object sender, EventArgs e)
        {
            if(tbInputTemperature.Text != "" && tbInputPressure.Text != "")
            {
                dgv.Rows.Add(dtpSelectedDate.Value.ToString(), tbInputTemperature.Text, tbInputPressure.Text);

                string writePath = "information.txt";

                try
                {
                    using (StreamWriter sw = new StreamWriter(writePath, false, System.Text.Encoding.Default))
                    {
                        sw.WriteLine(dtpSelectedDate.Value.ToString() +
                            " - temperature = " + tbInputTemperature.Text +
                            "; pressure = " + tbInputPressure.Text + ";");
                    }

                    MessageBox.Show("Запис успішно додано!");
                }
                catch (Exception exception)
                {
                    MessageBox.Show("Помилка! " + exception);
                }
            }
            else
            {
                MessageBox.Show("Заповніть усі поля!");
            }
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            for(int i = 0; i < dgv.Rows.Count - 1; i++)
            {
                if(dgv.Rows[i].Cells[0].Value.ToString() != dtpSelectedDate.Value.ToString())
                {
                    dgv.Rows[i].Visible = false;
                }
            }
        }
    }
}
