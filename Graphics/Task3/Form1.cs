﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private readonly ArrayList _squares = new ArrayList();
        private readonly Pen _pen = new Pen(Color.Black, 5);

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    {
                        var square = new Rectangle(e.X, e.Y, 20, 20);
                        _squares.Add(square);
                        Invalidate(square);
                        break;
                    }
                case MouseButtons.Right:
                    {
                        var squareNumber = 0;
                        foreach (Rectangle square in _squares)
                        {
                            squareNumber++;
                            if (square.Contains(e.X, e.Y))
                            {
                                MessageBox.Show($"Point inside square N{squareNumber}");
                            }
                        }
                        break;
                    }
            }
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            foreach (Rectangle square in _squares)
            {
                e.Graphics.DrawRectangle(_pen, square);
            }
            squareCountLabel.Text = $"There are [{_squares.Count}] squares";
        }
    }
}
