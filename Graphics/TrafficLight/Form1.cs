﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrafficLight
{
    public partial class Form1 : Form
    {
        private Timer _timerRed;
        private Timer _timerYellow;
        private Timer _timerGreen;
        private Timer _timerRedPedestrian;
        private Timer _timerGreenPedestrian;

        private Rectangle _ellipseRed;
        private Rectangle _ellipseYellow;
        private Rectangle _ellipseGreen;
        private Rectangle _pedestrianRed;
        private Rectangle _pedestrianGreen;

        Graphics graphics;

        private readonly Image _image = Image.FromFile("person.png");

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            graphics = CreateGraphics();

            _timerYellow = new Timer();
            _timerYellow.Tick += TimerYellowTick;
            _timerYellow.Interval = 3000;
            _timerYellow.Start();

            _timerGreen = new Timer();
            _timerGreen.Tick += TimerGreenTick;
            _timerGreen.Interval = 3200;
            _timerGreen.Start();

            _timerRed = new Timer();
            _timerRed.Tick += TimerRedTick;
            _timerRed.Interval = 3400;
            _timerRed.Start();

            _timerGreenPedestrian = new Timer();
            _timerGreen.Tick += TimerGreenPedestrianTick;
            _timerGreenPedestrian.Interval = 2000;
            _timerGreenPedestrian.Start();

            _timerRedPedestrian = new Timer();
            _timerRed.Tick += TimerRedPedestrianTick;
            _timerRedPedestrian.Interval = 3000;
            _timerRedPedestrian.Start();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Pen pen = new Pen(Color.Black, 2);

            var mainTrafficRectangle = new Rectangle(20, 20, 100, 220);

            graphics.DrawRectangle(pen, mainTrafficRectangle);

            _ellipseRed = new Rectangle(40, 30, 60, 60);
            graphics.FillEllipse(Brushes.Red, _ellipseRed);
            graphics.DrawEllipse(pen, _ellipseRed);

            _ellipseYellow = new Rectangle(40, 100, 60, 60);
            graphics.FillEllipse(Brushes.Black, _ellipseYellow);
            graphics.DrawEllipse(pen, _ellipseYellow);

            _ellipseGreen = new Rectangle(40, 170, 60, 60);
            graphics.FillEllipse(Brushes.Black, _ellipseGreen);
            graphics.DrawEllipse(pen, _ellipseGreen);

            var pedestrianRectangle = new Rectangle(150, 20, 70, 160);

            graphics.DrawRectangle(pen, pedestrianRectangle);

            _pedestrianRed = new Rectangle(165, 40, 40, 50);
            graphics.FillRectangle(Brushes.Red, _pedestrianRed);
            graphics.DrawImage(_image, _pedestrianRed);

            _pedestrianGreen = new Rectangle(165, 100, 40, 50);
            graphics.FillRectangle(Brushes.Gray, _pedestrianGreen);
            graphics.DrawImage(_image, _pedestrianGreen);
        }

        private void TimerRedTick(object sender, EventArgs e)
        {
            graphics.FillEllipse(Brushes.Red, _ellipseRed);
            graphics.FillEllipse(Brushes.Black, _ellipseYellow);
            graphics.FillEllipse(Brushes.Black, _ellipseGreen);
        }

        private void TimerYellowTick(object sender, EventArgs e)
        {
            graphics.FillEllipse(Brushes.Black, _ellipseRed);
            graphics.FillEllipse(Brushes.Yellow, _ellipseYellow);
            graphics.FillEllipse(Brushes.Black, _ellipseGreen);
        }

        private void TimerGreenTick(object sender, EventArgs e)
        {
            graphics.FillEllipse(Brushes.Black, _ellipseRed);
            graphics.FillEllipse(Brushes.Black, _ellipseYellow);
            graphics.FillEllipse(Brushes.Green, _ellipseGreen);
        }

        private void TimerRedPedestrianTick(object sender, EventArgs e)
        {
            graphics.FillRectangle(Brushes.Red, _pedestrianRed);
            graphics.DrawImage(_image, _pedestrianRed);
            graphics.FillRectangle(Brushes.Gray, _pedestrianGreen);
            graphics.DrawImage(_image, _pedestrianGreen);
        }

        private void TimerGreenPedestrianTick(object sender, EventArgs e)
        {
            var graphics = CreateGraphics();

            graphics.FillRectangle(Brushes.Gray, _pedestrianRed);
            graphics.DrawImage(_image, _pedestrianRed);
            graphics.FillRectangle(Brushes.Green, _pedestrianGreen);
            graphics.DrawImage(_image, _pedestrianGreen);
        }

        
    }
}
