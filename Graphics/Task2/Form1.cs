﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            var graphics = e.Graphics;
            graphics.FillRectangle(Brushes.White, ClientRectangle);
            graphics.FillRectangle(Brushes.Red, new Rectangle(10, 10, 50, 50));

            Brush linearGradientBrush =
                new LinearGradientBrush(new Rectangle(10, 60, 50, 50), Color.Blue, Color.White, 45);
            graphics.FillRectangle(linearGradientBrush, new Rectangle(10, 60, 50, 50));

            linearGradientBrush.Dispose();

            graphics.FillEllipse(Brushes.Aquamarine, new Rectangle(60, 20, 50, 30));

            graphics.FillPie(Brushes.Chartreuse, new Rectangle(60, 60, 50, 50), 90, 210);

            graphics.FillPolygon(Brushes.BlueViolet, new Point[]
            {
                new Point(110, 10),
                new Point(150, 10),
                new Point(160, 40),
                new Point(120, 20),
                new Point(120, 60),
            });
        }
    }
}
