﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            var graphics = e.Graphics;
            using (var blackPen = new Pen(Color.Black, 1))
            {
                for (var i = 0; i < ClientRectangle.Height; i += ClientRectangle.Height / 10)
                {
                    graphics.DrawLine(blackPen, new Point(0, 0), new Point(ClientRectangle.Width, i));
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
