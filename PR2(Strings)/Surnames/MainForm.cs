﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IZVP1_Krylov_29._09
{
    public partial class MainForm : Form
    {
        List<String> surnames = new List<string>();

        public MainForm()
        {
            InitializeComponent();
        }

        private void addSurnameBtn_Click(object sender, EventArgs e)
        {
            string surname = labelSurname.Text;

            if(surname != "")
            {
                surnames.Add(surname);
                rtb.Text += surname;
                rtb.Text += "\n";
            }

            labelSurname.Text = "";
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void sortBtn_Click(object sender, EventArgs e)
        {
            surnames.Sort();

            rtb.Text = "";

            foreach (string str in surnames)
            {
                rtb.Text += str;
                rtb.Text += "\n";
            }
        }

        private void findLargestSurnameButton_Click(object sender, EventArgs e)
        {
            string largestSurname = surnames[0];
            for(int i = 0; i < surnames.Count; i++)
            {
                if(largestSurname.Length < surnames[i].Length)
                {
                    largestSurname = surnames[i];
                }
            }

            labelLongestSurname.Text = "Найдовше прізвище: \n\t" + largestSurname;
        }
    }
}
