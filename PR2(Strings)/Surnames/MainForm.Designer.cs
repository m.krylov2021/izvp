﻿
namespace IZVP1_Krylov_29._09
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.rtb = new System.Windows.Forms.RichTextBox();
            this.labelSurname = new Guna.UI2.WinForms.Guna2TextBox();
            this.addSurnameBtn = new Guna.UI2.WinForms.Guna2Button();
            this.sortBtn = new Guna.UI2.WinForms.Guna2Button();
            this.labelLongestSurname = new System.Windows.Forms.Label();
            this.exitBtn = new Guna.UI2.WinForms.Guna2Button();
            this.findLargestSurnameButton = new Guna.UI2.WinForms.Guna2Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // rtb
            // 
            this.rtb.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtb.Location = new System.Drawing.Point(12, 12);
            this.rtb.Name = "rtb";
            this.rtb.Size = new System.Drawing.Size(316, 380);
            this.rtb.TabIndex = 0;
            this.rtb.Text = "";
            // 
            // labelSurname
            // 
            this.labelSurname.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelSurname.DefaultText = "";
            this.labelSurname.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.labelSurname.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.labelSurname.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.labelSurname.DisabledState.Parent = this.labelSurname;
            this.labelSurname.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.labelSurname.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.labelSurname.FocusedState.Parent = this.labelSurname;
            this.labelSurname.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSurname.ForeColor = System.Drawing.Color.Black;
            this.labelSurname.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.labelSurname.HoverState.Parent = this.labelSurname;
            this.labelSurname.Location = new System.Drawing.Point(369, 60);
            this.labelSurname.Name = "labelSurname";
            this.labelSurname.PasswordChar = '\0';
            this.labelSurname.PlaceholderText = "";
            this.labelSurname.SelectedText = "";
            this.labelSurname.ShadowDecoration.Parent = this.labelSurname;
            this.labelSurname.Size = new System.Drawing.Size(200, 38);
            this.labelSurname.TabIndex = 1;
            this.labelSurname.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // addSurnameBtn
            // 
            this.addSurnameBtn.Animated = true;
            this.addSurnameBtn.BorderRadius = 8;
            this.addSurnameBtn.CheckedState.Parent = this.addSurnameBtn;
            this.addSurnameBtn.CustomImages.Parent = this.addSurnameBtn;
            this.addSurnameBtn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.addSurnameBtn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.addSurnameBtn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.addSurnameBtn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.addSurnameBtn.DisabledState.Parent = this.addSurnameBtn;
            this.addSurnameBtn.FillColor = System.Drawing.Color.ForestGreen;
            this.addSurnameBtn.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.addSurnameBtn.ForeColor = System.Drawing.Color.White;
            this.addSurnameBtn.HoverState.Parent = this.addSurnameBtn;
            this.addSurnameBtn.Location = new System.Drawing.Point(380, 122);
            this.addSurnameBtn.Name = "addSurnameBtn";
            this.addSurnameBtn.ShadowDecoration.Parent = this.addSurnameBtn;
            this.addSurnameBtn.Size = new System.Drawing.Size(180, 50);
            this.addSurnameBtn.TabIndex = 2;
            this.addSurnameBtn.Text = "Додати прізвище";
            this.addSurnameBtn.Click += new System.EventHandler(this.addSurnameBtn_Click);
            // 
            // sortBtn
            // 
            this.sortBtn.Animated = true;
            this.sortBtn.BorderRadius = 8;
            this.sortBtn.CheckedState.Parent = this.sortBtn;
            this.sortBtn.CustomImages.Parent = this.sortBtn;
            this.sortBtn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.sortBtn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.sortBtn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.sortBtn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.sortBtn.DisabledState.Parent = this.sortBtn;
            this.sortBtn.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sortBtn.ForeColor = System.Drawing.Color.White;
            this.sortBtn.HoverState.Parent = this.sortBtn;
            this.sortBtn.Location = new System.Drawing.Point(380, 188);
            this.sortBtn.Name = "sortBtn";
            this.sortBtn.ShadowDecoration.Parent = this.sortBtn;
            this.sortBtn.Size = new System.Drawing.Size(180, 50);
            this.sortBtn.TabIndex = 3;
            this.sortBtn.Text = "Відсортувати прізвища";
            this.sortBtn.Click += new System.EventHandler(this.sortBtn_Click);
            // 
            // labelLongestSurname
            // 
            this.labelLongestSurname.AutoSize = true;
            this.labelLongestSurname.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelLongestSurname.Location = new System.Drawing.Point(381, 326);
            this.labelLongestSurname.Name = "labelLongestSurname";
            this.labelLongestSurname.Size = new System.Drawing.Size(175, 21);
            this.labelLongestSurname.TabIndex = 4;
            this.labelLongestSurname.Text = "Найдовше прізвище:";
            // 
            // exitBtn
            // 
            this.exitBtn.CheckedState.Parent = this.exitBtn;
            this.exitBtn.CustomImages.Parent = this.exitBtn;
            this.exitBtn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.exitBtn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.exitBtn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.exitBtn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.exitBtn.DisabledState.Parent = this.exitBtn;
            this.exitBtn.FillColor = System.Drawing.Color.White;
            this.exitBtn.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.exitBtn.ForeColor = System.Drawing.Color.White;
            this.exitBtn.HoverState.Parent = this.exitBtn;
            this.exitBtn.Image = ((System.Drawing.Image)(resources.GetObject("exitBtn.Image")));
            this.exitBtn.ImageSize = new System.Drawing.Size(30, 30);
            this.exitBtn.Location = new System.Drawing.Point(581, 6);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.ShadowDecoration.Parent = this.exitBtn;
            this.exitBtn.Size = new System.Drawing.Size(42, 45);
            this.exitBtn.TabIndex = 5;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // findLargestSurnameButton
            // 
            this.findLargestSurnameButton.Animated = true;
            this.findLargestSurnameButton.BorderRadius = 8;
            this.findLargestSurnameButton.CheckedState.Parent = this.findLargestSurnameButton;
            this.findLargestSurnameButton.CustomImages.Parent = this.findLargestSurnameButton;
            this.findLargestSurnameButton.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.findLargestSurnameButton.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.findLargestSurnameButton.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.findLargestSurnameButton.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.findLargestSurnameButton.DisabledState.Parent = this.findLargestSurnameButton;
            this.findLargestSurnameButton.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.findLargestSurnameButton.ForeColor = System.Drawing.Color.White;
            this.findLargestSurnameButton.HoverState.Parent = this.findLargestSurnameButton;
            this.findLargestSurnameButton.Location = new System.Drawing.Point(380, 254);
            this.findLargestSurnameButton.Name = "findLargestSurnameButton";
            this.findLargestSurnameButton.ShadowDecoration.Parent = this.findLargestSurnameButton;
            this.findLargestSurnameButton.Size = new System.Drawing.Size(180, 50);
            this.findLargestSurnameButton.TabIndex = 6;
            this.findLargestSurnameButton.Text = "Визначити найдовше прізвище";
            this.findLargestSurnameButton.Click += new System.EventHandler(this.findLargestSurnameButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(396, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 21);
            this.label1.TabIndex = 7;
            this.label1.Text = "Введіть прізвище:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(631, 412);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.findLargestSurnameButton);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.labelLongestSurname);
            this.Controls.Add(this.sortBtn);
            this.Controls.Add(this.addSurnameBtn);
            this.Controls.Add(this.labelSurname);
            this.Controls.Add(this.rtb);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Головна";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtb;
        private Guna.UI2.WinForms.Guna2TextBox labelSurname;
        private Guna.UI2.WinForms.Guna2Button addSurnameBtn;
        private Guna.UI2.WinForms.Guna2Button sortBtn;
        private System.Windows.Forms.Label labelLongestSurname;
        private Guna.UI2.WinForms.Guna2Button exitBtn;
        private Guna.UI2.WinForms.Guna2Button findLargestSurnameButton;
        private System.Windows.Forms.Label label1;
    }
}

