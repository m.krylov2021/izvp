﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Students
{
    public partial class MainForm : Form
    {
        List<Student> studentsList = new List<Student>();

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if(tbMarks.Text != "" && tbName.Text != "" && tbSurname.Text != "")
            {
                Student student = new Student();
                student.setName(tbName.Text);
                student.setSurname(tbSurname.Text);
                student.setMarks(Array.ConvertAll(tbMarks.Text.Split(','), uint.Parse));
                studentsList.Add(student);

                tbMarks.Text = "";
                tbName.Text = "";
                tbSurname.Text = "";

                MessageBox.Show("Запис успішно додано!");
            }
            else
            {
                MessageBox.Show("Заповніть усі поля!");
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnViewRating_Click(object sender, EventArgs e)
        {
            Data.Value = studentsList;
            RatingForm form = new RatingForm();
            form.Show();
        }
    }
}
