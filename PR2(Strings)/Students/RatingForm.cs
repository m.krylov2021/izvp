﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Students
{
    public partial class RatingForm : Form
    {
        List<Student> studentsList = Data.Value;

        public RatingForm()
        {
            InitializeComponent();
        }

        private void RatingForm_Load(object sender, EventArgs e)
        {
            foreach(Student student in studentsList)
            {
                bool flag = true;

                foreach (uint i in student.getMarks())
                {
                    if (i < 4) flag = false;
                }

                if (flag) dgv.Rows.Add(student.getName(), student.getSurname(), string.Join(" ", student.getMarks()));
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
