﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Students
{
    class Student
    {
        private string name { get; set; }
        private string surname { get; set; }
        private uint[] marks { get; set; }

        public void setName(string name) { this.name = name; }
        public void setSurname(string surname) { this.surname = surname; }
        public void setMarks(uint[] marks) { this.marks = marks; }
        public string getName() { return name; }
        public string getSurname() { return surname; }
        public uint[] getMarks() { return marks; }

        public Student() {}
    }
}
