﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Residents
{
    class Resident
    {
        private string name;
        private string surname;
        private string middleName;
        private string homeAddress;
        private DateTime dateOfBirth;

        public void setName(string name) { this.name = name; }
        public void setSurname(string surname) { this.surname = surname; }
        public void setMiddleNAme(string middleName) { this.middleName = middleName; }
        public void setDateOfBirth(DateTime dateOfBirth) { this.dateOfBirth = dateOfBirth; }
        public string getName() { return name; }
        public string getSurname() { return surname; }
        public string getMiddleNAme() { return middleName; }
        public DateTime getDateOfBirth() { return dateOfBirth; }

        public Resident(DateTime dateOfBirth, string name = "", string surname = "",
            string middleName = "", string homeAddress = "")
        {
            this.name = name;
            this.surname = surname;
            this.middleName = middleName;
            this.homeAddress = homeAddress;
            this.dateOfBirth = dateOfBirth;
        }
        public Resident() {}
    }
}
