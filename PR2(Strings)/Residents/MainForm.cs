﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Residents
{
    public partial class MainForm : Form
    {
        List<Resident> listOfResidents = new List<Resident>();

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if(tbName.Text != "" && tbSurname.Text != "" && tbMiddleName.Text != "" && tbAddress.Text != "" && dtpBirth.Value != DateTime.Now)
            {
                Resident resident = new Resident(dtpBirth.Value, tbName.Text, tbSurname.Text,
                tbMiddleName.Text, tbAddress.Text);
                listOfResidents.Add(resident);

                dgv.Rows.Add(tbName.Text, tbSurname.Text,
                    tbMiddleName.Text, tbAddress.Text, dtpBirth.Value.ToShortDateString());

                tbName.Text = "";
                tbSurname.Text = "";
                tbMiddleName.Text = "";
                tbAddress.Text = "";
                dtpBirth.Value = DateTime.Now;
            }
            else
            {
                MessageBox.Show("Заповніть усі поля!");
            }
        }

        private void btnCreateList_Click(object sender, EventArgs e)
        {
            if (tbSearchAddress.Text != "")
            {
                for (int i = 0; i < dgv.Rows.Count; i++)
                {
                    if (dgv.Rows[i].Cells[3].Value.ToString() != tbSearchAddress.Text && 
                        DateTime.Parse(dgv.Rows[i].Cells[4].Value.ToString()).Year >= 18)
                    {
                        dgv.Rows.RemoveAt(i);
                        dgv.Refresh();
                    }
                }

                tbSearchAddress.Text = "";

            }
            else
            {
                MessageBox.Show("Введіть адресу!");
            }
        }
    }
}
