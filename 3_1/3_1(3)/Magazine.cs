﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _3_1_3_
{
    class Magazine : Printing
    {
        private ulong publicationNumber;

        public Magazine() {}
        
        public Magazine(String name, uint year, uint numberOfCopiesInLibrary, 
            uint numberOfCopiesFromReaders, ulong publicationNumber)
            : base(name, year, numberOfCopiesInLibrary, numberOfCopiesFromReaders) {

            this.publicationNumber = publicationNumber;
        }
    }
}
