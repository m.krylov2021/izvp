﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _3_1_3_
{
    class Book : Printing
    {
        private String authors;
        private String publisherName;
        private uint amountOfPages;

        public Book() {}

        public Book(String name, uint year, uint numberOfCopiesInLibrary,
            uint numberOfCopiesFromReaders, String authors,
            String publisherName, uint amountOfPages)
            : base(name, year, numberOfCopiesInLibrary, numberOfCopiesFromReaders) {

            this.authors = authors;
            this.publisherName = publisherName;
            this.amountOfPages = amountOfPages;
        }
    }
}
