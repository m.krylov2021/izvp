﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _3_1_3_
{
    class Newspaper : Printing
    {
        private DateTime dateOfIssuance;

        public Newspaper() {}

        public Newspaper(String name, uint year, uint numberOfCopiesInLibrary,
            uint numberOfCopiesFromReaders, DateTime dateOfIssuance)
            : base(name, year, numberOfCopiesInLibrary, numberOfCopiesFromReaders) {

            this.dateOfIssuance = dateOfIssuance;
        }
    }
}
