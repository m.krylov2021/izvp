﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _3_1_3_
{
    abstract class Printing
    {
        private String name;
        private uint year;
        private uint numberOfCopiesInLibrary;
        private uint numberOfCopiesFromReaders;

        public Printing() {}

        public Printing(String name, uint year, uint numberOfCopiesInLibrary, uint numberOfCopiesFromReaders) {
            this.name = name;
            this.year = year;
            this.numberOfCopiesInLibrary = numberOfCopiesInLibrary;
            this.numberOfCopiesFromReaders = numberOfCopiesFromReaders;
        }

        public uint addCopiesToLibrary(uint additionalCopies)
        {
            numberOfCopiesInLibrary += additionalCopies;

            return numberOfCopiesInLibrary;
        }

        public uint addCopiesToReaders(uint additionalCopies)
        {
            numberOfCopiesFromReaders += additionalCopies;

            return numberOfCopiesFromReaders;
        }

        public uint deleteCopiesFromLibrary(uint numberOfSubtractedCopies)
        {
            numberOfCopiesInLibrary -= (numberOfCopiesInLibrary > numberOfSubtractedCopies) 
                ? numberOfSubtractedCopies 
                : 0;

            return numberOfCopiesInLibrary;
        }

        public uint deleteCopiesFromReaders(uint numberOfSubtractedCopies)
        {
            numberOfCopiesFromReaders -= (numberOfCopiesFromReaders > numberOfSubtractedCopies)
                ? numberOfSubtractedCopies
                : 0;

            return numberOfCopiesFromReaders;
        }
    }
}
