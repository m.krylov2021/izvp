﻿using System;

namespace _3_1_2_
{
    class Program
    {
        static void Main(string[] args)
        {
            Triangle rightTriangle = new RightTriangle(5, 4, 4, 5, 2, 5);

            Console.WriteLine(rightTriangle.calculatePerimetr());
            Console.WriteLine(rightTriangle.calculateSquare());
        }
    }
}
