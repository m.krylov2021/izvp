﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _3_1_2_
{
    abstract class Triangle
    {
        private int x1;
        private int y1;
        private int x2;
        private int y2;
        private int x3;
        private int y3;

        private double firstSideLength;
        private double secondSideLength;
        private double thirdSideLength;

        private double perimetr;
        private double square;

        public Triangle() {}
        public Triangle(int x1, int y1, int x2, int y2, int x3, int y3) {
            this.x1 = x1;
            this.y1 = y1;
            this.x2 = x2;
            this.y2 = y2;
            this.x3 = x3;
            this.y3 = y3;
            
            firstSideLength = calculateSideLength(x1, y1, x2, y2);
            secondSideLength = calculateSideLength(x2, y2, x3, y3);
            thirdSideLength = calculateSideLength(x3, y3, x1, y1);

            perimetr = calculatePerimetr();
            square = calculateSquare();
        }
        
        public Triangle(double firstSideLength, double secondSideLength, double thirdSideLength) {
            this.firstSideLength = firstSideLength;
            this.secondSideLength = secondSideLength;
            this.thirdSideLength = thirdSideLength;

            perimetr = calculatePerimetr();
            square = calculateSquare();
        }

        public static double calculateSideLength(int x1, int y1, int x2, int y2)
        {
            return Math.Sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
        }

        public double calculatePerimetr()
        {
            return firstSideLength + secondSideLength + thirdSideLength;
        }

        public double calculateSquare()
        {
            return Math.Sqrt(perimetr/2 * (perimetr/2 - firstSideLength) * (perimetr/2 - secondSideLength) * (perimetr/2 - thirdSideLength));
        }
    }
}
