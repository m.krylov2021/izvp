﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_1_4_
{
    class Car
    {
        private ulong carNumber;
        private Color carColor;
        private String model;
        private String ownerFirstName;
        private String ownerLastName;
        private String ownerPhoneNumber;

        public Car() {}

        public Car(ulong carNumber, Color carColor,
            String model,String ownerFirstName,
            String ownerLastName, String ownerPhoneNumber) {

            this.carNumber = carNumber;
            this.carColor = carColor;
            this.model = model;
            this.ownerFirstName = ownerFirstName;
            this.ownerLastName = ownerFirstName;
            this.ownerPhoneNumber = ownerPhoneNumber;
        }
    }

    enum Color
    {
        YELLOW, GREEN, RED
    }
}
