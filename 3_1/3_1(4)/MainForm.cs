﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _3_1_4_
{
    public partial class MainForm : Form
    {
        List<Car> carsList = new List<Car>();

        public MainForm()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Color carColor = rbYellow.Checked
                ? Color.YELLOW
                : (rbGreen.Checked ? Color.GREEN : Color.RED);

            carsList.Add(new Car(ulong.Parse(tbCarNum.Text), carColor, tbModel.Text,
                tbFirstName.Text, tbLastName.Text, mtbPhoneNum.Text));

            listView.Items.Add(tbCarNum.Text);

            clearInputFields();
        }

        private void clearInputFields()
        {
            tbFirstName.Text = "";
            tbLastName.Text = "";
            mtbPhoneNum.Text = "";
            tbCarNum.Text = "";
            tbModel.Text = "";
            rbYellow.Checked = false;
            rbGreen.Checked = false;
            rbRed.Checked = false;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            ListView.SelectedIndexCollection collection = listView.SelectedIndices;

            if (collection.Count != 0)
            {
                carsList.Remove(carsList[collection[0]]);
                listView.Items.RemoveAt(collection[0]);
            }
        }
    }
}
