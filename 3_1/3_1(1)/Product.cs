﻿using System;
using System.Collections.Generic;
using System.Text;

/*
 * A class that describes a product.
 * 
 * Date: 18.11.2021
 * Author: Krylov M.V.
 */

namespace _3_1
{
    class Product
    {
        private String name;
        private String unitsOfMeasure;
        private uint availableQuantity;
        private double pricePerUnit;
        private DateTime expiryDate;

        public Product() {}

        public int getNumberDaysToExpirationDate()
        {
            return expiryDate.Subtract(DateTime.Now).Days;
        }
        
        public double getProductsPrice(uint quantity)
        {
            return quantity * pricePerUnit;
        }

        public uint IncreasingProducts(uint additionalQuantity)
        {
            if(additionalQuantity > 0)
            {
                availableQuantity += additionalQuantity;
            }

            return availableQuantity;
        }

        public void setExpiryDate(DateTime newExpiryDate)
        {
            expiryDate = newExpiryDate;
        }

    }
}
