﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Train
{
    public partial class MainForm : Form
    {
        struct Train
        {
            public string nameOfEndPoint;
            public ulong numOfTrain;
            public DateTime timeOfDeparture;
        }

        List<Train> listOfTrains = new List<Train>(8);

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if(tbEndOfRoute.Text != "" && tbNumOfTrain.Text != "" &&
                listOfTrains.Count < 9 && dtp.Value >= DateTime.Now)
            {
                Train train = new Train();
                train.nameOfEndPoint = tbEndOfRoute.Text;
                train.numOfTrain = UInt64.Parse(tbNumOfTrain.Text);
                train.timeOfDeparture = dtp.Value;

                listOfTrains.Add(train);

                dgv.Rows.Add(tbEndOfRoute.Text, tbNumOfTrain.Text, dtp.Value.ToShortDateString());

                tbEndOfRoute.Text = "";
                tbNumOfTrain.Text = "";
                dtp.Value = DateTime.Now;
            }
        }

        private void btnFindTrain_Click(object sender, EventArgs e)
        {
            if(tbSearch.Text != "")
            {
                for(int i = 0; i < dgv.Rows.Count - 1; i++)
                {
                    if(dgv.Rows[i].Cells[0].Value.ToString() != tbSearch.Text)
                    {
                        dgv.Rows[i].Visible = false;
                    }
                }
            }

            tbSearch.Text = "";
        }
    }
}
