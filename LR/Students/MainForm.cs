﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Students
{
    public partial class MainForm : Form
    {
        List<Student> studentsList = new List<Student>();

        public MainForm()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if(tbName.Text != "" && tbSurname.Text != "" && tbMarks.Text != "")
            {
                Student student = new Student();
                student.setName(tbName.Text);
                student.setSurname(tbSurname.Text);
                student.setMarks(Array.ConvertAll(tbMarks.Text.Split(','), uint.Parse));
                studentsList.Add(student);

                string scholarship = "Відсутня";
                uint sum = 0;

                foreach(uint mark in student.getMarks())
                {
                    sum += mark;
                }

                double avg = sum / 5;

                if (avg >= 4.5)
                {
                    scholarship = "Підвищ.";
                }
                else if (avg >= 4) scholarship = "Звичайна";

                dgv.Rows.Add(student.getName(), student.getSurname(), string.Join(" ", student.getMarks()), scholarship);

                tbMarks.Text = "";
                tbName.Text = "";
                tbSurname.Text = "";

                MessageBox.Show("Запис успішно додано!");
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if(tbSearch.Text != "")
            {
                for(int i = 0; i < dgv.Rows.Count - 1; i++)
                {
                    if(dgv.Rows[i].Cells[1].Value.ToString() != tbSearch.Text)
                    {
                        dgv.Rows[i].Visible = false;
                    }
                }
            }

            tbSearch.Text = "";
        }
    }
}
