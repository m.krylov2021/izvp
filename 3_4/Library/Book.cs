﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library
{
    class Book
    {
        private String bookName;
        private String author;
        private String publisher;

        public Book(String bookName, String author, String publisher)
        {
            this.bookName = bookName;
            this.author = author;
            this.publisher = publisher;
        }

        public String getBookName()
        {
            return bookName;
        }

        public String getAuthor()
        {
            return author;
        }

        public String getPublisher()
        {
            return publisher;
        }

        public override string ToString()
        {
            return "book name: " + bookName + "; book author: " + author + "; book publisher: " + publisher + ".";
        }
    }
}
