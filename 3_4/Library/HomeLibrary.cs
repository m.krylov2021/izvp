﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library
{
    class HomeLibrary
    {
        private List<Book> booksList;

        public HomeLibrary(List<Book> booksList)
        {
            this.booksList = booksList;
        }

        public void sortBooksByName()
        {
            booksList.Sort(delegate(Book currentBook, Book nextBook)
                            {
                                return currentBook.getBookName().CompareTo(nextBook.getBookName());
                            });
        }

        public void sortBooksByAuthor()
        {
            booksList.Sort(delegate (Book currentBook, Book nextBook)
                            {
                                return currentBook.getAuthor().CompareTo(nextBook.getAuthor());
                            });
        }

        public void sortBooksByPublisher()
        {
            booksList.Sort(delegate (Book currentBook, Book nextBook)
                            {
                                return currentBook.getPublisher().CompareTo(nextBook.getPublisher());
                            });
        }

        public void printBookList()
        {
            foreach(Book book in booksList)
            {
                Console.WriteLine(book);
            }
        }
    }
}
