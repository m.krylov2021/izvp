﻿using System;
using System.Collections.Generic;

namespace Library
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Book> booksList = new List<Book>();

            booksList.Add(new Book("B", "book author", "book publisher"));
            booksList.Add(new Book("A", "Miguel de Cervantes", "Ecmo"));
            booksList.Add(new Book("C", "Jack London", "Ecmo"));
            

            var homeLibrary = new HomeLibrary(booksList);

            homeLibrary.sortBooksByName();
            homeLibrary.printBookList();
        }
    }
}
